﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text.RegularExpressions;
using Azzysa.Batch.Web.Constants;
using Azzysa.Batch.Web.Extensions;
using Azzysa.Batch.Web.Models.Files;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Azzysa.Batch.Web
{
    public class FcsClient : IFileClient
    {
        public Stream GetFile(FcsTicket ticket)
        {
            byte[] response;
            using (var client = new System.Net.WebClient())
            {
                response = client.UploadValues(ticket.Action, new NameValueCollection { { Fcs.JobTicketFromParameter, ticket.Ticket } });
            }

            return new MemoryStream(response);
        }

        public string CheckinFile(FcsTicket ticket, Stream fileStream)
        {
            try
            {
                var data = new PostData();
                data.Params.Add(new PostDataParam(Fcs.JobTicketFromParameter, ticket.Ticket, PostDataParamType.Field));
                data.Params.Add(new PostDataParam($"{Fcs.FileFormParameter}0", System.Text.Encoding.Default.GetString(fileStream.ToByteArray()), PostDataParamType.File) { FileName = @"Test.pdf" });

                var request = (HttpWebRequest)WebRequest.Create(ticket.Action);
                request.ContentType = MimeTypes.MultipartFormDataWithBoundary + PostData.Boundary;
                request.Method = RequestMethod.POST.ToString();

                byte[] buffer = System.Text.Encoding.Default.GetBytes(data.GetPostData());

                request.ContentLength = buffer.Length;

                var stream = request.GetRequestStream();
                stream.Write(buffer, 0, buffer.Length);
                stream.Close();
                
                var response = (HttpWebResponse)request.GetResponse();

                var responseHtml = new StreamReader(response.GetResponseStream()).ReadToEnd();

                return GetJobReceiptFromHtml(responseHtml);

            }
            catch (WebException e)
            {
                // todo log error here
                throw;
            }
        }

        private string GetJobReceiptFromHtml(string html)
        {
            var pattern = @"<input type='hidden' name='__fcs__jobReceipt' value='(.*)'>";
            var matches = Regex.Match(html, pattern);
            return matches.Groups[1].Value;
        }
    }
}