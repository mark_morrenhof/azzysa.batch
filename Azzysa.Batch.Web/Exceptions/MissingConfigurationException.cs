﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Azzysa.Batch.Web.Exceptions
{
    public class MissingConfigurationException : Exception
    {
        public MissingConfigurationException(List<string> configNames) : base(GetMessage(configNames)) { }

        public MissingConfigurationException(string configName) : base(GetMessage(configName))
        {

        }

        private static string GetMessage(string configName)
        {
            return $"Can't find dependent AppSetting with configuration name '{configName}'!";
        }

        private static string GetMessage(List<string> configNames)
        {
            if (configNames.Count == 1)
                return GetMessage(configNames.First());

            var sb = new StringBuilder();
            sb.AppendLine("Can't find dependent AppSetting with the following configuration names:");
            sb.AppendLine();

            foreach (var s in configNames)
                sb.AppendLine($"\t{ s }");

            return sb.ToString();
        }
    }
}
