﻿using System;
using System.Runtime.Serialization;

namespace Azzysa.Batch.Web.Exceptions
{
    public class AzzysaWebException : Exception
    {
        private string message = string.Empty;

        public override string Message => message;
        public string ExceptionType { get; set; }

        public AzzysaWebException(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                return;

            message = info.GetString("message");
            ExceptionType = info.GetString("exceptionType");
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            if (info == null)
                return;

            info.AddValue("message", Message);
            info.AddValue("exceptionType", ExceptionType);
        }
    }
}
