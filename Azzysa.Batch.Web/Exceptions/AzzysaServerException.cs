﻿using System;
using System.Net;

namespace Azzysa.Batch.Web.Exceptions
{
    public class AzzysaServerException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }

        private AzzysaServerException(Exception exc, HttpStatusCode code) : base("An unknown error occurred while contacting the Azzysa server!", exc)
        {
            StatusCode = code;
        }

        public static AzzysaServerException Create(string message, HttpStatusCode code = HttpStatusCode.InternalServerError) => Create(new Exception(message), code);

        public static AzzysaServerException Create(Exception exc, HttpStatusCode code = HttpStatusCode.InternalServerError)
        {
            if (exc is AzzysaServerException)
                return exc as AzzysaServerException;
            return new AzzysaServerException(exc, code);
        }
    }
}
