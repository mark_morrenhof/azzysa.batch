﻿using System.Runtime.Serialization;

namespace Azzysa.Batch.Web.Exceptions
{
    public class EnoviaObjectNotFoundException : AzzysaWebException
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string Revision { get; set; }

        public EnoviaObjectNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            if (info == null)
                return;

            Type = info.GetString("type");
            Name = info.GetString("name");
            Revision = info.GetString("revision");
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            if (info == null)
                return;

            info.AddValue("type", Type);
            info.AddValue("name", Name);
            info.AddValue("revision", Revision);
        }
    }
}
