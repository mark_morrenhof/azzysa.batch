﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;

namespace Azzysa.Batch.Web.Exceptions
{
    public class ExceptionHandler
    {
        public static Exception BuildException(Exception exc)
        {
            if (exc is WebException)
                return BuildWebException(exc as WebException);
            else
                return exc;
        }

        private static Exception BuildWebException(WebException exc)
        {
            if (exc.Response == null || !(exc.Response is HttpWebResponse))
                return exc;

            var response = exc.Response as HttpWebResponse;

            if (response.ContentLength == 0)
                return AzzysaServerException.Create(exc);

            string message = null;

            var contentEncoding = response.Headers[Constants.HttpHeaders.ContentEncoding];

            if (contentEncoding != null && contentEncoding.Equals(Constants.ContentEncodings.GZip))
            {
                using (var stream = response.GetResponseStream())
                using (var gs = new GZipStream(stream, CompressionMode.Decompress))
                using (var ms = new MemoryStream())
                {
                    gs.CopyTo(ms);
                    message = Encoding.Default.GetString(ms.ToArray());
                }
            }
            else
            {
                using (var stream = response.GetResponseStream())
                using (var ms = new MemoryStream())
                {
                    stream.CopyTo(ms);
                    message = Encoding.Default.GetString(ms.ToArray());
                }
            }

            if (!string.IsNullOrEmpty(message) && response.StatusCode == HttpStatusCode.BadRequest)
            {
                try
                {
                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<AzzysaWebException>(message);
                    var type = Type.GetType($"Vanderlande.Web.Exceptions.{ result.ExceptionType }");

                    if (type != null)
                        return (AzzysaWebException)Newtonsoft.Json.JsonConvert.DeserializeObject(message, type);
                    else
                        return result;
                }
                catch
                {
                    return AzzysaServerException.Create(message,response.StatusCode);
                }
            }
            else
                return AzzysaServerException.Create(message, response.StatusCode);
        }
    }
}
