﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using Azzysa.Batch.Web.Constants;
using Azzysa.Batch.Web.Extensions;
using Azzysa.Batch.Web.Models;
using Azzysa.Batch.Web.Models.Dimensions;
using Azzysa.Batch.Web.Models.Documents;
using Azzysa.Batch.Web.Models.Engineering;
using Azzysa.Batch.Web.Models.Files;
using Azzysa.Batch.Web.Routes;
using File = Azzysa.Batch.Web.Models.Files;

namespace Azzysa.Batch.Web
{
    internal class BatchConnector : IBatchConnector
    {
        private WebClient client;

        internal static BatchConnector Instance { get; set; }

        internal BatchConnector(string url)
        {
            this.client = new WebClient(url);

            #region Routes

            //Test
            ApiRouteCollection.Add(BatchApiRoutes.Test);

            //File
            ApiRouteCollection.Add(BatchApiRoutes.GetFcsTicketCheckout);
            ApiRouteCollection.Add(BatchApiRoutes.GetFcsTicketCheckin);
            ApiRouteCollection.Add(BatchApiRoutes.FcsJobReceipt);
            ApiRouteCollection.Add(BatchApiRoutes.RelatedFiles);

            #endregion
        }

        #region Create Instance
        public static IBatchConnector CreateInstance(string url)
        {
            Instance = new BatchConnector(url);
            return Instance;
        }
        #endregion

        #region Test

        public string Test()
        {
            return client.Get<string>(ApiRouteCollection.GetRoute(BatchApiRoutes.Test));
        }

        #endregion

        #region Public Members

        public void CheckInFile(Stream fileStream, FcsCheckinRequest request)
        {
            var checkinTicket = GetFcsTicketCheckin(request);

            var jobReceipt = PerformCheckin(checkinTicket, fileStream, request.Files.FirstOrDefault()?.Name);

            SendJobReceipt(new FcsJobReceipt { Receipt = jobReceipt });
        }

        public Stream CheckOutFile(FcsCheckoutRequest request)
        {
            var checkoutTicket = GetFcsTicketCheckout(request);

            return GetFile(checkoutTicket);
        }

        public Part GetRelatedFiles(Part part, IList<string> types = null) => GetRelatedFiles(new RelatedFilesRequest() {Name = part.Name, Revision = part.Revision, Type = part.Type, TypeNames = types});

        private Part GetRelatedFiles(RelatedFilesRequest request, string folderName = null)
        {
            var part = client.Post<Part>(ApiRouteCollection.GetRoute(BatchApiRoutes.RelatedFiles), request);

            var fileDataForFcsTicket = GetRelatedFileData(part);

            if (fileDataForFcsTicket == null || !fileDataForFcsTicket.Any())
            {
                return part;
            }
            
            var fcsTicket = GetFcsTicketCheckout(new FcsCheckoutRequest
            {
                Files = fileDataForFcsTicket,
                Zip = true,
            });

            folderName = string.IsNullOrWhiteSpace(folderName) ? Guid.NewGuid().ToString() : folderName;
            var pathToSave = Path.Combine(Path.GetTempPath(), folderName);

            using (var inputStream = GetFile(fcsTicket))
            {
                var zipArchive = new ZipArchive(inputStream, ZipArchiveMode.Read);
                zipArchive.ExtractToDirectory(pathToSave, true);
            }

            AssignFilePaths(part, pathToSave);

            return part;
        }

        #endregion

        #region File Communication

        private FcsTicket GetFcsTicketCheckout(FcsCheckoutRequest fcsTicketRequest)
        {
            return client.Post<FcsTicket>(ApiRouteCollection.GetRoute(BatchApiRoutes.GetFcsTicketCheckout), fcsTicketRequest);
        }

        private FcsTicket GetFcsTicketCheckin(FcsCheckinRequest fcsTicketRequest)
        {
            return client.Post<FcsTicket>(ApiRouteCollection.GetRoute(BatchApiRoutes.GetFcsTicketCheckin), fcsTicketRequest);
        }

        private string SendJobReceipt(FcsJobReceipt receipt)
        {
            return client.Post<string>(ApiRouteCollection.GetRoute(BatchApiRoutes.FcsJobReceipt), receipt);
        }

        #endregion
        private Stream GetFile(FcsTicket ticket)
        {
            byte[] response;
            using (var client = new System.Net.WebClient())
            {
                response = client.UploadValues(ticket.Action, new NameValueCollection { { Fcs.JobTicketFromParameter, ticket.Ticket } });
            }

            return new MemoryStream(response);
        }

        private string PerformCheckin(FcsTicket ticket, Stream fileStream, string fileName)
        {
            try
            {
                var data = new PostData();
                data.Params.Add(new PostDataParam(Fcs.JobTicketFromParameter, ticket.Ticket, PostDataParamType.Field));
                data.Params.Add(new PostDataParam($"{Fcs.FileFormParameter}0", System.Text.Encoding.Default.GetString(fileStream.ToByteArray()), PostDataParamType.File) { FileName = fileName });

                var request = (HttpWebRequest)WebRequest.Create(ticket.Action);
                request.ContentType = MimeTypes.MultipartFormDataWithBoundary + PostData.Boundary;
                request.Method = RequestMethod.POST.ToString();

                byte[] buffer = System.Text.Encoding.Default.GetBytes(data.GetPostData());

                request.ContentLength = buffer.Length;

                var stream = request.GetRequestStream();
                stream.Write(buffer, 0, buffer.Length);
                stream.Close();

                var response = (HttpWebResponse)request.GetResponse();

                var responseHtml = new StreamReader(response.GetResponseStream()).ReadToEnd();

                return GetJobReceiptFromHtml(responseHtml);

            }
            catch (WebException e)
            {
                // todo log error here
                throw;
            }
        }

        private IList<FileData> GetRelatedFileData(Part part)
        {
            var result = new List<FileData>();

            if (part != null)
            {
                var partSpecifications = part.RelatedObjects?.Where(ro => ro is CADModel);

                if (partSpecifications != null && partSpecifications.Any())
                {
                    foreach (var partSpecification in partSpecifications)
                    {
                        result.AddRange(partSpecification.RelatedFiles.Select(rf => new FileData() { Name = rf.Name, Format = rf.Format, ObjectName = partSpecification.Name, ObjectRevision = partSpecification.Revision, ObjectType = partSpecification.Type}));
                    }
                }

                if (part.EBOM != null && part.EBOM.Any())
                {
                    foreach (var ebom in part.EBOM)
                    {
                        result.AddRange(GetRelatedFileData(ebom.ConnectedObject));
                    }
                }

                if (part.RelatedPartgroup != null)
                {
                    result.AddRange(GetRelatedFileData(part.RelatedPartgroup));
                }
            }

            return result;
        }

        private IList<FileData> GetRelatedFileData(PartGroup partGroup)
        {
            var result = new List<FileData>();

            var partSpecifications = partGroup?.RelatedObjects?.Where(ro => ro is CADModel);

            if (partSpecifications != null && partSpecifications.Any())
            {
                foreach (var partSpecification in partSpecifications)
                {
                    result.AddRange(partSpecification.RelatedFiles.Select(rf => new FileData() { Name = rf.Name, Format = rf.Format, ObjectName = partSpecification.Name, ObjectRevision = partSpecification.Revision, ObjectType = partSpecification.Type }));
                }
            }

            return result;
        }
        private string GetJobReceiptFromHtml(string html)
        {
            var pattern = @"<input type='hidden' name='__fcs__jobReceipt' value='(.*)'>";
            var matches = Regex.Match(html, pattern);
            return matches.Groups[1].Value;
        }

        private void AssignFilePaths(Part part, string path)
        {
            part?.RelatedPartgroup?.RelatedObjects?.SelectMany(ro => ro.RelatedFiles).ToList().ForEach(f => f.Path = path);

            part?.RelatedObjects?.SelectMany(ro => ro.RelatedFiles).ToList().ForEach(f => f.Path = path);

            part?.EBOM?.ForEach(ebom =>
            {
                AssignFilePaths(ebom.ConnectedObject, path);
                ebom.ConnectedObject?.RelatedObjects?.SelectMany(ro => ro.RelatedFiles).ToList().ForEach(f => f.Path = path);
            });
        }
    }
}
