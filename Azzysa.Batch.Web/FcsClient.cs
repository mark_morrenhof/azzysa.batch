﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text.RegularExpressions;
using Azzysa.Batch.Web.Extensions;
using Azzysa.Batch.Web.Models.Files;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Azzysa.Batch.Web
{
    public class FcsClient : IFileClient
    {
        public Stream GetFile(FcsTicket ticket)
        {
            byte[] response;
            using (var client = new System.Net.WebClient())
            {
                response = client.UploadValues(ticket.Action, new NameValueCollection { { "__fcs__jobTicket", ticket.Ticket } });
            }

            return new MemoryStream(response);
        }

        public string CheckinFile(FcsTicket ticket, Stream fileStream)
        {
            try
            {
                var data = new PostData();
                data.Params.Add(new PostDataParam("__fcs__jobTicket", ticket.Ticket, PostDataParamType.Field));
                data.Params.Add(new PostDataParam("bfile0", System.Text.Encoding.Default.GetString(fileStream.ToByteArray()), PostDataParamType.File) { FileName = @"Test.pdf" });

                var request = (HttpWebRequest)WebRequest.Create(ticket.Action);
                request.ContentType = "multipart/form-data; boundary=" + PostData.Boundary;
                request.Method = HttpMethod.Post.ToString();

                byte[] buffer = System.Text.Encoding.Default.GetBytes(data.GetPostData());

                request.ContentLength = buffer.Length;

                var stream = request.GetRequestStream();
                stream.Write(buffer, 0, buffer.Length);
                stream.Close();
                
                var response = (HttpWebResponse)request.GetResponse();

                var responseHtmlString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                var pattern = @"<input type='hidden' name='__fcs__jobReceipt' value='(.*)'>";
                var matches = Regex.Match(responseHtmlString, pattern);
                return matches.Groups[1].Value;

            }
            catch (WebException e)
            {
                // todo log error here
                throw;
            }
        }
    }
}