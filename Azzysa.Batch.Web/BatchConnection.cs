﻿namespace Azzysa.Batch.Web
{
    public static class BatchConnection
    {
        public static IBatchConnector Register(string url)
        {
            return BatchConnector.CreateInstance(url);
        }
    }
}
