﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Azzysa.Batch.Web.Routes
{
    public class ApiRoute
    {
        private ApiRouteType routeType;
        private List<string> values = new List<string>();

        public ApiRoute(ApiRouteType route, params string[] values)
        {
            this.routeType = route;
            this.values = new List<string>(values);
        }

        public Uri GetRouteUrl(Uri url)
        {
            if (routeType.Arguments.Count != values.Count)
                return null;

            Uri uri = GetRouteEndpoint(url, routeType.Route);

                foreach(var s in values)
                    uri = uri.AddComponents(s);

            return uri;
        }

        private Uri GetRouteEndpoint(Uri url, Enum route)
        {
            var fi = route.GetType().GetField(route.ToString());
            var attributes = (ApiRouteAttribute[])fi.GetCustomAttributes(typeof(ApiRouteAttribute), false);

            if (attributes.Length > 0)
                return url.AddComponents(attributes[0].Endpoint);

            throw new InvalidOperationException($"ApiRoute '{ route }: { route.ToString() }' doesn't have a endpoint assigned");
        }
    }
}
