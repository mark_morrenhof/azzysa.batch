﻿namespace Azzysa.Batch.Web.Routes
{
    public static class ApiRoutePathArguments
    {
        public const string Type = "type";
        public const string Name = "name";
        public const string Revision = "revision";
        public const string Id = "id";
        public const string User = "user";
    }

    public static class ApiRouteQueryStringArguments
    {
    }
}
