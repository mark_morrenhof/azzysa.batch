﻿namespace Azzysa.Batch.Web.Routes
{
    public class ApiRouteAttribute : System.Attribute
    {
        public string Endpoint { get; set; }

        public ApiRouteAttribute(string endpoint)
        {
            Endpoint = endpoint;
        }
    }
}
