﻿namespace Azzysa.Batch.Web.Routes
{
    internal enum BatchApiRoutes
    {
        [ApiRoute("api/test")]
        Test,

        //File
        [ApiRoute("api/fcs/ticket/checkin/get")]
        GetFcsTicketCheckin = 10001,
            
        [ApiRoute("api/fcs/ticket/checkout/get")]
        GetFcsTicketCheckout = 10002,

        [ApiRoute("api/fcs/complete/checkin")]
        FcsJobReceipt = 10003,
        
        [ApiRoute("api/get/relatedFiles")]
        RelatedFiles = 20001,
    }
}
