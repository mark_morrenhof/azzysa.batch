﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Azzysa.Batch.Web.Routes
{
    public static class ApiRouteCollection
    {
        private static Dictionary<Enum, List<ApiRouteType>> routes = new Dictionary<Enum, List<ApiRouteType>>();

        public static void Add(Enum route, params string[] args)
        {
            if (!routes.ContainsKey(route))
                routes.Add(route, new List<ApiRouteType>());
            routes[route].Add(new ApiRouteType(route, args));
        }

        public static ApiRoute GetRoute(Enum route, params string[] values)
        {
            if (!routes.ContainsKey(route))
                return null;

            var type = routes[route].FirstOrDefault(r => r.Arguments.Count == values.Length);

            if (type == null)
                return null;

            return new ApiRoute(type, values);
        }
    }
}
