﻿using System;
using System.Collections.Generic;

namespace Azzysa.Batch.Web.Routes
{
    public class ApiRouteType
    {
        public Enum Route { get; set; }
        public List<string> Arguments { get; set; } = new List<string>();

        public ApiRouteType(Enum route, params string[] args)
        {
            Route = route;
            Arguments = new List<string>(args);
        }
    }
}
