﻿using System.IO;
using Azzysa.Batch.Web.Models.Files;

namespace Azzysa.Batch.Web
{
    public interface IFileClient
    {
        Stream GetFile(FcsTicket ticket);

        string CheckinFile(FcsTicket ticket, Stream fileStream);
    }
}