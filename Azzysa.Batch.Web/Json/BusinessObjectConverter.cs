﻿using System;
using Azzysa.Batch.Web.Models;
using Azzysa.Batch.Web.Models.Documents;
using Azzysa.Batch.Web.Models.Engineering;
using Newtonsoft.Json.Linq;

namespace Azzysa.Batch.Web.Json
{
    public class BusinessObjectConverter : JsonCreationConverter<BusinessObject>
    {
        protected override BusinessObject Create(Type objectType, JObject jObject)
        {
            if (FieldExists("t", jObject))
            {
                var type = jObject["t"].Value<string>();

                switch (type)
                {
                    case PartGroup.TypeName:
                        return new PartGroup();
                    case Part.TypeName:
                        return new Part();
                    case SWAssemblyFamily.TypeName:
                        return new SWAssemblyFamily();
                    case SWComponentFamily.TypeName:
                        return new SWComponentFamily();
                    case SWDrawing.TypeName:
                        return new SWDrawing();
                    case Person.TypeName:
                        return new Person();
                }
            }

            return base.Create(objectType, jObject);

            //return (BusinessObject)Activator.CreateInstance(objectType);
        }

        private bool FieldExists(string fieldName, JObject jObject)
        {
            return jObject[fieldName] != null;
        }
    }
}
