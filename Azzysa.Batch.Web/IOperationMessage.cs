﻿namespace Azzysa.Batch.Web
{
    /// <summary>
    /// Interface thar represents 
    /// </summary>
    public interface IOperationMessage
    {
        string Operation { get; set; }

        string Site { get; set; }

        string Parameters { get; set; }
    }
}
