﻿namespace Azzysa.Batch.Web.Constants
{
    public class HttpHeaders
    {
        public const string ContentEncoding = "Content-Encoding";
        public const string AcceptEncoding = "Accept-Encoding";
        public const string ContentType = "Content-Type";
    }
}
