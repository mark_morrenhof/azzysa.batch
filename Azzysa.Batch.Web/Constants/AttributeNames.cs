﻿namespace Azzysa.Batch.Web.Constants
{
    public class AttributeNames
    {
        public const string UnitOfMeasure = "Unit of Measure";
        public const string FindNumber = "Find Number";
        public const string SparePartType = "VI Spare Part Type";
        public const string Quantity = "Quantity";
        public const string Measure = "VI Measure";
        public const string CADQuantity = "VI CAD Quantity";
        public const string CADMeasure = "VI CAD Measure";
        public const string FindNumberLock = "VI CAD Find Number Lock";
        public const string ConfigurationForDrawing = "VI CAD Configuration For Drawing";
        public const string IBCHeaderInfo = "VI IBC Header Info";
        public const string SparePartCriticality = "VI Spare Part Criticality";
        public const string ResponsibleDesignEngineer = "Responsible Design Engineer";
        public const string Description1 = "Description 1";
        public const string Description2 = "Description 2";
        public const string OnHold = "VI On Hold";
    }
}
