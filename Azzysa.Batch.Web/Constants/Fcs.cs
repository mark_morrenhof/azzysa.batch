﻿namespace Azzysa.Batch.Web.Constants
{
    public class Fcs
    {
        public const string JobTicketFromParameter = "__fcs__jobTicket";
        public const string JobReceiptFormParameter = "__fcs__jobReceipt";
        public const string FileFormParameter = "bfile";
    }
}