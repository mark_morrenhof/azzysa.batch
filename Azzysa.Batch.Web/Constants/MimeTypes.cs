﻿namespace Azzysa.Batch.Web.Constants
{
    public class MimeTypes
    {
        public const string Json = "application/json";

        public const string FormData = "application/x-www-form-urlencoded";

        public const string MultipartFormDataWithBoundary = "multipart/form-data; boundary=";
    }
}
