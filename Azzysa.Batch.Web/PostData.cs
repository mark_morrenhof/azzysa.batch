﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Azzysa.Batch.Web
{
    public class PostData
    {

        public static string Boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");

        private List<PostDataParam> _params;

        public List<PostDataParam> Params
        {
            get { return _params; }
            set { _params = value; }
        }

        public PostData()
        {
            _params = new List<PostDataParam>();
        }

        public string GetPostData()
        { 
            StringBuilder sb = new StringBuilder();
            foreach (PostDataParam p in _params)
            {
                sb.AppendLine("--" + Boundary);

                if (p.Type == PostDataParamType.File)
                {
                    sb.AppendLine($"Content-Disposition: form-data; name=\"{p.Name}\"; filename=\"{p.FileName}\"");
                    sb.AppendLine("Content-Type: application/pdf");
                    sb.AppendLine();
                    sb.AppendLine(p.Value);
                }
                else
                {
                    sb.AppendLine($"Content-Disposition: form-data; name=\"{p.Name}\"");
                    sb.AppendLine();
                    sb.AppendLine(p.Value);
                }
            }

            sb.Append("--" + Boundary + "--");

            return sb.ToString();
        }
    }

    public enum PostDataParamType
    {
        Field,
        File
    }

    public class PostDataParam
    {
        public PostDataParam(string name, string value, PostDataParamType type)
        {
            Name = name;
            Value = value;
            Type = type;
        }

        public string Name { get; set; }
        public string FileName { get; set; }
        public string Value { get; set; }
        public PostDataParamType Type { get; set; }
    }
}
