﻿namespace Azzysa.Batch.Web
{
    internal enum RequestMethod
    {
        POST,
        GET
    }

    public enum UpdateAction : int
    {
        None = 0,
        Add = 1,
        Update = 2,
        Delete = 3
    }
}