﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azzysa.Batch.Web
{
    public interface IJobAgentInfo
    {
        Guid Id { get; set; }
        IEnumerable<string> Operations { get; set; }
        string Version { get; set; }
        string Site { get; set; }
        string Port { get; set; }
    }
}
