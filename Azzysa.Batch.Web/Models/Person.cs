﻿using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models
{
    public class Person : BusinessObject
    {
        public const string TypeName = "Person";

        [JsonProperty("d")]
        public string DisplayName { get; set; }

        [JsonProperty("i")]
        public string Initials { get; set; }

        internal Person()
        {
        }
    }
}