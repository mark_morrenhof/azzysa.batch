﻿using System;

namespace Azzysa.Batch.Web.Models
{
    public class BackgroundTask
    {
        public string TaskId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
