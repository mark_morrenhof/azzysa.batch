﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models.Classification
{
    public class ClassificationPath : List<ClassificationPath.Item>
    {
        public override string ToString()
        {
            var path = string.Empty;

            foreach (var i in this)
            {
                if (string.IsNullOrEmpty(path))
                    path = i.Name;
                else
                    path = $"{ path } > { i }";
            }

            return path;
        }

        public class Item
        {
            [JsonProperty("id")]
            public string Id { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }

            public override string ToString()
            {
                return Name;
            }

            internal Item() { }
        }
    }
}
