﻿using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models.Documents
{
    public class SWAssemblyFamily : CADModel
    {
        public const string TypeName = "SW Assembly Family";

        [JsonIgnore]
        public new string Type => TypeName;

        internal SWAssemblyFamily() : base() { }

        public SWAssemblyFamily(string name, string revision) : base(TypeName, name, revision)
        {
        }
    }
}
