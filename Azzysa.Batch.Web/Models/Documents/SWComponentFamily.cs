﻿using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models.Documents
{
    public class SWComponentFamily : CADModel
    {
        public const string TypeName = "SW Component Family";

        [JsonIgnore]
        public new string Type => TypeName;

        internal SWComponentFamily() : base() { }

        public SWComponentFamily(string name, string revision) : base(TypeName, name, revision)
        {
        }
    }
}
