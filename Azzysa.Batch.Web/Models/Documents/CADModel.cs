﻿using System.Collections.Generic;
using Azzysa.Batch.Web.Models.Engineering;
using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models.Documents
{
    public class CADModel : BusinessObject
    {
        [JsonProperty("rel")]
        public List<BaseComponent> RelatedComponents { get; set; } = new List<BaseComponent>();

        internal CADModel() : base() { }

        public CADModel(string type, string name, string revision) : base(type, name, revision) { }
    }
}
