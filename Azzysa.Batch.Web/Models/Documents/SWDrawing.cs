﻿using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models.Documents
{
    public class SWDrawing : CADModel
    {
        public const string TypeName = "SW Drawing";

        [JsonIgnore]
        public new string Type => TypeName;

        internal SWDrawing() : base() { }

        public SWDrawing(string name, string revision) : base(TypeName, name, revision)
        {
        }
    }
}