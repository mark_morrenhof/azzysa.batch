﻿namespace Azzysa.Batch.Web.Models.Dimensions
{
    public class Unit
    {
        public bool IsDefault { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
