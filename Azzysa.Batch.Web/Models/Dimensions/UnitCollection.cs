﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Azzysa.Batch.Web.Models.Dimensions
{
    public class UnitCollection : List<Unit>
    {
        public Unit this[string name]
        {
            get
            {
                return this.FirstOrDefault(u => u.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
            }
        }

        public void Add(string name, string label, bool isDefault = false)
        {
            Add(new Unit {
                Name = name,
                Label = label,
                IsDefault = isDefault
            });
        }
    }
}
