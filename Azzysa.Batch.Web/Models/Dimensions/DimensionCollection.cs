﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Azzysa.Batch.Web.Models.Dimensions
{
    public class DimensionCollection : List<Dimension>
    {
        public Dimension this[string name]
        {
            get
            {
                return this.FirstOrDefault(d => d.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
            }
        }
    }
}
