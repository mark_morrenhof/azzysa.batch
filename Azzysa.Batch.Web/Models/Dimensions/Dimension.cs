﻿using System.Linq;

namespace Azzysa.Batch.Web.Models.Dimensions
{
    public class Dimension
    {
        public string Name { get; }
        public UnitCollection Units { get; } = new UnitCollection();
        public Unit DefaultUnit => Units.FirstOrDefault(u => u.IsDefault);

        public Dimension(string name)
        {
            Name = name;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
