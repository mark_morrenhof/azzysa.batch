﻿using System.Collections.Generic;
using Azzysa.Batch.Web.Models.Engineering;

namespace Azzysa.Batch.Web.Models.Parameters
{
    public class RelatedFilesParameters : IOperationParameters
    {
        public Part Part { get; set; }

        public IList<string> Types { get; set; }
    }
}