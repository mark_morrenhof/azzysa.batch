﻿namespace Azzysa.Batch.Web.Models.Parameters
{
    public class TestParameters : IOperationParameters
    {
        public string First { get; set; }

        public string Second { get; set; }
    }
}