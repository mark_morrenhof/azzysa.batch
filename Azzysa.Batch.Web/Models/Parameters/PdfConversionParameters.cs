﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azzysa.Batch.Web.Models.Parameters
{
    public class PdfConversionParameters : IOperationParameters
    {
        public string SourceFileName { get; set; }
        public string SourceFormat { get; set; }
        public string OutputFileName { get; set; }
        public string OutputFormat { get; set; }
        public string ObjectId { get; set; }
        public string ObjectType { get; set; }
        public string ObjectName { get; set; }
        public string ObjectRevision { get; set; }
    }
   
}
