﻿namespace Azzysa.Batch.Web.Models
{
    public class BackgroundTaskStatus : BackgroundTask
    {
        public string Status { get; set; }
        public decimal? Progress { get; set; }
    }
}
