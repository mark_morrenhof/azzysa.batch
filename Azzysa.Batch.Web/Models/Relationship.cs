﻿using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models
{
    public class Relationship<T>
    {
        [JsonProperty("id")]
        public string Id { get; internal set; }

        [JsonProperty("c")]
        public T ConnectedObject { get; set; }

        [JsonProperty("a")]
        public AttributeCollection Attributes { get; set; } = new AttributeCollection();

        [JsonProperty("u")]
        public UpdateAction? Action { get; set; }
    }
}
