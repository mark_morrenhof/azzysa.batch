﻿using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models
{
    public class EnoviaKey
    {
        #region Properties
        [JsonProperty("t")]
        public string Type { get; set; }
        [JsonProperty("n")]
        public string Name { get; set; }
        [JsonProperty("r")]
        public string Revision { get; set; }
        #endregion

        internal EnoviaKey() : base() { }

        public EnoviaKey(string type, string name, string revision)
        {
            Type = type;
            Name = name;
            Revision = revision;
        }

        public string ToDisplayString()
        {
            return $"{ Type }.{ Name }.{ Revision }";
        }

        public EnoviaKey ToEnoviaKey()
        {
            return new EnoviaKey(Type, Name, Revision);
        }
    }
}
