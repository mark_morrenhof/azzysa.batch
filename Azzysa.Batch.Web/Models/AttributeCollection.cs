﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Azzysa.Batch.Web.Models
{
    public class AttributeCollection : Dictionary<string, string>
    {
        internal T GetAttribute<T>(string name)
        {
            var stringValue = GetAttribute(name);

            Type t = typeof(T);

            if (t == typeof(string))
                return (T)Convert.ChangeType(stringValue, t);

            Type nullableType = Nullable.GetUnderlyingType(t);
            Type baseType = nullableType == null ? t : nullableType;

            if (string.IsNullOrEmpty(stringValue) && nullableType != null)
                return default(T);
            return (T)Convert.ChangeType(stringValue, baseType, CultureInfo.InvariantCulture);
        }

        internal string GetAttribute(string name)
        {
            if (ContainsKey(name))
                return this[name];
            return null;
        }

        internal void SetAttribute<T>(string name, T value)
        {
            SetAttribute(name, value.ToString());
        }

        internal void SetAttribute(string name, string value)
        {
            if (!ContainsKey(name))
                Add(name, value);
            else
                this[name] = value;
        }
    }
}
