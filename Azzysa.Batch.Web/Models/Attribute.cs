﻿using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models
{
    public class Attribute
    {
        [JsonProperty("n")]
        public string Name { get; set; }

        [JsonProperty("t")]
        public string Type { get; set; }

        [JsonProperty("d")]
        public string Dimension { get; set; }

        [JsonProperty("v")]
        public string Value { get; set; }
    }
}
