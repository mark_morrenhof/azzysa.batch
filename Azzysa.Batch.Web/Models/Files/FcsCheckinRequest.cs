﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Azzysa.Batch.Web.Models.Files
{
    public class FcsCheckinRequest
    {
        [JsonProperty("files")]
        public IEnumerable<FileData> Files { get; set; }
        [JsonProperty("location")]
        public string Location { get; set; }
    }
}