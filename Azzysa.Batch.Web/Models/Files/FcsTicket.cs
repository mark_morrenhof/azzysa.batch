﻿using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models.Files
{
    public class FcsTicket
    {
        [JsonProperty("action")]
        public string Action { get; set; }

        [JsonProperty("__fcs__jobTicket")]
        public string Ticket { get; set; }
    }
}