﻿using System.Collections.Generic;
using Azzysa.Batch.Web.Models.Engineering;
using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models.Files
{
    public class RelatedFilesRequest
    {
        [JsonProperty("t")]
        public string Type { get; set; }

        [JsonProperty("n")]
        public string Name { get; set; }

        [JsonProperty("r")]
        public string Revision { get; set; }
        
        [JsonProperty("tn")]
        public IEnumerable<string> TypeNames { get; set; }
    }
}