﻿using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models.Files
{
    public class FileData
    {
        [JsonProperty("t")]
        public string ObjectType { get; set; }
        [JsonProperty("n")]
        public string ObjectName { get; set; }
        [JsonProperty("r")]
        public string ObjectRevision { get; set; }
        [JsonProperty("objectId")]
        public string ObjectId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("format")]
        public string Format { get; set; }
        [JsonProperty("lock")]
        public string Lock { get; set; }
        [JsonProperty("append")]
        public string Append { get; set; }
    }
}