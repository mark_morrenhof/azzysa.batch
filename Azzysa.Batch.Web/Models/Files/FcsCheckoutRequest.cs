﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Azzysa.Batch.Web.Models.Files
{
    public class FcsCheckoutRequest
    {
        [JsonProperty("files")]
        public IEnumerable<FileData> Files { get; set; }
        [JsonProperty("zip")]
        public bool Zip { get; set; }
        [JsonProperty("zipName")]
        public string ZipName { get; set; }
        [JsonProperty("location")]
        public string Location { get; set; }
    }
}