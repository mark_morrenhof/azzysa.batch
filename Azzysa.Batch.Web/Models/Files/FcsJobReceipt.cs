﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Azzysa.Batch.Web.Models.Files
{
    public class FcsJobReceipt
    {
        [JsonProperty("jobReceipt")]
        public string Receipt { get; set; }
    }
}