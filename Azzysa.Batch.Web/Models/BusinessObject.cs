﻿using System.Collections.Generic;
using Azzysa.Batch.Web.Models.Engineering;
using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models
{
    [JsonConverter(typeof(Json.BusinessObjectConverter))]
    public class BusinessObject : EnoviaKey
    {
        [JsonProperty("d")]
        public string Description { get; set; }
        [JsonProperty("s")]
        public string State { get; set; }

        [JsonProperty("attr")]
        public AttributeCollection Attributes { get; set; } = new AttributeCollection();

        [JsonProperty("ro")]
        internal BusinessObjectCollection RelatedObjects { get; set; } = new BusinessObjectCollection();
        
        [JsonProperty("rf")]
        public IEnumerable<File> RelatedFiles { get; set; }

        [JsonProperty("a")]
        public UpdateAction? Action { get; set; }

        internal BusinessObject() : base() { }

        public BusinessObject(string type, string name, string revision) : base(type, name, revision)
        {
        }

        [JsonProperty("e")]
        public bool Exists { get; set; }

        public class BusinessObjectCollection : List<BusinessObject>
        {
            public BusinessObjectCollection()
            {
            }

            public BusinessObjectCollection(IEnumerable<BusinessObject> collection) : base(collection)
            {
            }

            public BusinessObjectCollection(int capacity) : base(capacity)
            {
            }
        }
    }
}
