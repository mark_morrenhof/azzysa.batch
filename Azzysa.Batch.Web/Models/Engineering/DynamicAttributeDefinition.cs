﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models.Engineering
{
    public class DynamicAttributeDefinition
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("unit")]
        public string Unit { get; set; }
        [JsonProperty("symbol")]
        public string Symbol { get; set; }
        [JsonProperty("dimension")]
        public string Dimension { get; set; }
        [JsonProperty("type")]
        public string ValueType { get; set; }
        [JsonProperty("uic")]
        public bool UsedInCADModel { get; set; }
        [JsonProperty("id")]
        public List<string> SmarTeamIds { get; set; }

        public override bool Equals(object obj)
        {
            if(obj.GetType() != typeof(DynamicAttributeDefinition))
                return base.Equals(obj);
            return (obj as DynamicAttributeDefinition).Name.Equals(Name);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}