﻿using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models.Engineering
{
    public class DynamicAttribute
    {
        [JsonProperty("n")]
        public string Name { get; set; }
        [JsonProperty("v")]
        public string Value { get; set; }
        [JsonProperty("a")]
        public string UnitOfMeasure { get; set; }
        [JsonProperty("uic")]
        public bool UsedInCADModel { get; set; }
    }
}
