﻿using System.Collections;
using System.Collections.Generic;
using Azzysa.Batch.Web.Models.Classification;
using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models.Engineering
{
    public class BaseComponent<T> : BaseComponent
    {
        [JsonProperty("pr")]
        public T PreviousRevision { get; set; }

        internal BaseComponent() : base() { }

        public BaseComponent(string type, string name, string revision) : base(type, name, revision)
        {
        }
    }

    public class BaseComponent : BusinessObject
    {
        [JsonProperty("c")]
        public ClassificationPath Classification { get; set; } = new ClassificationPath();

        internal BaseComponent() : base() { }

        public BaseComponent(string type, string name, string revision) : base(type, name, revision)
        {
        }
    }
}