﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models.Engineering
{
    public class Part : BaseComponent<Part>
    {
        public const string TypeName = "Part";

        [JsonIgnore]
        public new string Type => TypeName;

        [JsonProperty("rpg")]
        public PartGroup RelatedPartgroup { get; set; }
        [JsonProperty("ebom")]
        public List<EBOM> EBOM { get; set; } = new List<EBOM>();
        [JsonProperty("dattr")]
        public List<DynamicAttribute> DynamicAttributes { get; set; } = new List<DynamicAttribute>();
        [JsonProperty("cat")]
        public bool? IsCatalogPart { get; internal set; }

        #region Attributes
        [JsonIgnore]
        public string UnitOfMeasure
        {
            get
            {
                return Attributes.GetAttribute(Constants.AttributeNames.UnitOfMeasure);
            }
            set
            {
                Attributes.SetAttribute(Constants.AttributeNames.UnitOfMeasure, value);
            }
        }

        [JsonIgnore]
        public string SparePartType
        {
            get
            {
                return Attributes.GetAttribute(Constants.AttributeNames.SparePartType);
            }
            set
            {
                Attributes.SetAttribute(Constants.AttributeNames.SparePartType, value);
            }
        }

        [JsonIgnore]
        public string ConfigurationForDrawing
        {
            get
            {
                return Attributes.GetAttribute(Constants.AttributeNames.ConfigurationForDrawing);
            }
            set
            {
                Attributes.SetAttribute(Constants.AttributeNames.ConfigurationForDrawing, value);
            }
        }
        #endregion

        internal Part() : base() { }

        public Part(string name) : base(TypeName, name, null) { }

        public Part(string name, string revision) : base(TypeName, name, revision)
        {
        }
    }
}
