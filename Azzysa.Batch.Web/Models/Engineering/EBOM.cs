﻿using System;
using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models.Engineering
{
    public class EBOM : Relationship<Part>
    {
        #region Attributes
        [JsonIgnore]
        public double? Quantity
        {
            get
            {
                return Attributes.GetAttribute<double?>(Constants.AttributeNames.Quantity);
            }
            set
            {
                Attributes.SetAttribute(Constants.AttributeNames.Quantity, value);
            }
        }

        [JsonIgnore]
        public double? Measure
        {
            get
            {
                return Attributes.GetAttribute<double?>(Constants.AttributeNames.Measure);
            }
            set
            {
                Attributes.SetAttribute(Constants.AttributeNames.Measure, value);
            }
        }

        [JsonIgnore]
        public string FindNumber
        {
            get
            {
                return Attributes.GetAttribute(Constants.AttributeNames.FindNumber);
            }
            set
            {
                Attributes.SetAttribute(Constants.AttributeNames.FindNumber, value);
            }
        }

        [JsonIgnore]
        public double? CADQuantity
        {
            get
            {
                return Attributes.GetAttribute<double?>(Constants.AttributeNames.CADQuantity);
            }
            set
            {
                Attributes.SetAttribute(Constants.AttributeNames.CADQuantity, value);
            }
        }

        [JsonIgnore]
        public double? CADMeasure
        {
            get
            {
                return Attributes.GetAttribute<double?>(Constants.AttributeNames.CADMeasure);
            }
            set
            {
                Attributes.SetAttribute(Constants.AttributeNames.CADMeasure, value);
            }
        }

        [JsonIgnore]
        public bool? FindNumberLock
        {
            get
            {
                return Attributes.GetAttribute<bool?>(Constants.AttributeNames.FindNumberLock);
            }
            set
            {
                Attributes.SetAttribute(Constants.AttributeNames.FindNumberLock, value);
            }
        }

        [JsonIgnore]
        public double? SparePartCriticality
        {
             get
            {
                return Attributes.GetAttribute<double?>(Constants.AttributeNames.SparePartCriticality);
            }
            set
            {
                Attributes.SetAttribute(Constants.AttributeNames.SparePartCriticality, value);
            }
        }
        #endregion

        internal EBOM() : base() { }

        public EBOM(string id)
        {
            Id = id;
        }
    }
}
