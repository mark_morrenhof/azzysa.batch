﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Models.Engineering
{
    public class PartGroup : BaseComponent<PartGroup>
    {
        public const string TypeName = "VI Part Group";

        [JsonIgnore]
        public new string Type => TypeName;
        
        [JsonProperty("dad")]
        public DynamicAttributeDefinitionCollection DynamicAttributeDefinitions { get; set; } = new DynamicAttributeDefinitionCollection();

        [JsonProperty("parts")]
        public List<Part> RelatedParts { get; set; } = new List<Part>();

        #region Attributes
        public string IBCHeaderInfo
        {
            get
            {
                return Attributes.GetAttribute(Constants.AttributeNames.IBCHeaderInfo);
            }
            set
            {
                Attributes.SetAttribute(Constants.AttributeNames.IBCHeaderInfo, value);
            }
        }

        [JsonIgnore]
        public string Description1
        {
            get { return Attributes.GetAttribute(Constants.AttributeNames.Description1); }
            set { Attributes.SetAttribute(Constants.AttributeNames.Description1, value); }
        }

        [JsonIgnore]
        public string Description2
        {
            get { return Attributes.GetAttribute(Constants.AttributeNames.Description2); }
            set { Attributes.SetAttribute(Constants.AttributeNames.Description2, value); }
        }

        [JsonIgnore]
        public Person ResponsibleDesignEngineer
        {
            get
            {
                var user = Attributes.GetAttribute(Constants.AttributeNames.ResponsibleDesignEngineer);

                if (!string.IsNullOrWhiteSpace(user))
                    return RelatedObjects.FirstOrDefault(
                        o => o.GetType() == typeof(Person) && ((Person)o).Name == user) as Person;
                return null;
            }
            set
            {
                if (value == null)
                    Attributes.Remove(Constants.AttributeNames.ResponsibleDesignEngineer);
                else
                    Attributes.SetAttribute(Constants.AttributeNames.ResponsibleDesignEngineer, value.Name);
            }
        }
        #endregion

        internal PartGroup() : base() { }

        public PartGroup(string name) : base(TypeName, name, null) { }

        public PartGroup(string name, string revision) : base(TypeName, name, revision)
        {
        }
    }
}
