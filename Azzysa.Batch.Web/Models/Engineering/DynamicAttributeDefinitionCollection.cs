﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Azzysa.Batch.Web.Models.Engineering
{
    public class DynamicAttributeDefinitionCollection : List<DynamicAttributeDefinition>
    {
        public DynamicAttributeDefinitionCollection() { }
        public DynamicAttributeDefinitionCollection(IEnumerable<DynamicAttributeDefinition> collection) : base(collection) { }

        public DynamicAttributeDefinition this[string name] => GetDynamicAttributeDefinition(name);

        public DynamicAttributeDefinition GetDynamicAttributeDefinition(string dimension, string symbol)
        {
            return this.FirstOrDefault(d => d.Dimension.Equals(dimension, StringComparison.OrdinalIgnoreCase) && d.Symbol.Equals(symbol, StringComparison.OrdinalIgnoreCase));
        }

        public DynamicAttributeDefinition GetDynamicAttributeDefinition(int smarteamId)
        {
            return this.FirstOrDefault(d => d.SmarTeamIds.Contains(smarteamId.ToString()));
        }

        public DynamicAttributeDefinition GetDynamicAttributeDefinition(string name)
        {
            return this.FirstOrDefault(d => d.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
        }
    }
}
