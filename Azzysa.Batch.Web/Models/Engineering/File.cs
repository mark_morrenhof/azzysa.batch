﻿using System.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Azzysa.Batch.Web.Models.Engineering
{
    public class File
    {
        [JsonProperty("fn")]
        public string Name { get; set; }
        [JsonProperty("ff")]
        public string Format { get; set; }
        public string Path { get; set; }
    }
}