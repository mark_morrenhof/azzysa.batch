﻿using System.Collections.Generic;

namespace System.Linq
{
    public static class ListExtensions
    {
        public static List<L> SelectAsList<T,L>(this IEnumerable<T> list, Func<T,L> query)
        {
            return list.Select(query).ToList();
        }

        public static List<T> WhereAsList<T>(this IEnumerable<T> list, Func<T, bool> query)
        {
            return list.Where(query).ToList();
        }
    }
}
