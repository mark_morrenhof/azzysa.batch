﻿using System.IO;
using System.IO.Compression;

namespace Azzysa.Batch.Web.Extensions
{
    public static class ZipArchiveExtensions
    {
        public static void ExtractToDirectory(this ZipArchive archive, string directory, bool overwrite = false)
        {
            if (!overwrite)
            {
                archive.ExtractToDirectory(directory);
                return;
            }
            foreach (ZipArchiveEntry file in archive.Entries)
            {
                string completeFileName = Path.Combine(directory, file.FullName);
                string directoryName = Path.GetDirectoryName(completeFileName);

                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);

                if (file.Name != string.Empty)
                    file.ExtractToFile(completeFileName, true);
            }
        }
    }

}