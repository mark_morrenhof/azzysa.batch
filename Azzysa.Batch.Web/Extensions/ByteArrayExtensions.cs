﻿using Azzysa.Batch.Web.Compression;

namespace System
{
    public static class ByteArrayExtensions
    {
        public static bool IsCompressed(this byte[] data)
        {
            return GZipCompression.IsCompressed(data);
        }
    }
}