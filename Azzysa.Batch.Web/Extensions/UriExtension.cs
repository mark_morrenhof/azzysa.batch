﻿using System.Collections.Generic;
using System.Linq;

namespace System
{
    public static class UriExtension
    {
        public static Uri AddComponents(this Uri url, params string[] components)
        {
            var uri = url;

            foreach (var s in components)
            {
                if (string.IsNullOrEmpty(s))
                    continue;

                var c = s.Trim('\\', '/');

                if (string.IsNullOrEmpty(c))
                    continue;

                uri = new Uri($"{uri}/{c}");
            }

            return uri;
        }
    }
}
