﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Extensions
{
    public static class OperationMessageExtensions
    {
        public static T GetParameters<T>(this IOperationMessage message)
        {
            return JsonConvert.DeserializeObject<T>(message.Parameters);
        }
    }
}
