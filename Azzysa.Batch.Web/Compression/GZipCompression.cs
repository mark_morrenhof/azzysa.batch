﻿using System.IO;
using System.Linq;

namespace Azzysa.Batch.Web.Compression
{
    public class GZipCompression
    {
        private static byte[] GZipHeaderBytes = { 0x1f, 0x8b, 8, 0, 0, 0, 0, 0, 4, 0 };
        private static byte[] GZipLevel10HeaderBytes = { 0x1f, 0x8b, 8, 0, 0, 0, 0, 0, 2, 0 };

        public static byte[] Compress(byte[] data)
        {
            using (var ms = new MemoryStream())
            {
                using (var s = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Compress, true))
                    s.Write(data, 0, data.Length);

                return ms.ToArray();
            }
        }

        public static byte[] Decompress(byte[] data)
        {
            using (var stream = new MemoryStream(data))
            {
                using (var gs = new System.IO.Compression.GZipStream(stream, System.IO.Compression.CompressionMode.Decompress))
                using (var ms = new MemoryStream())
                {
                    gs.CopyTo(ms);
                    return ms.ToArray();
                }
            }
        }

        public static bool IsCompressed(byte[] data)
        {
            var yes = data.Length > 10;

            if (!yes)
                return false;

            var header = data.Take(10);

            return header.SequenceEqual(GZipHeaderBytes) || header.SequenceEqual(GZipLevel10HeaderBytes);
        }
    }
}