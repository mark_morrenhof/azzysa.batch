﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;
using Azzysa.Batch.Web.Constants;
using Azzysa.Batch.Web.Exceptions;
using Azzysa.Batch.Web.Models.Files;
using Azzysa.Batch.Web.Routes;
using Newtonsoft.Json;

namespace Azzysa.Batch.Web
{
    public class WebClient
    {
        private Uri url;

        public WebClient(string url)
        {
            this.url = new Uri(url.TrimEnd('/'));
        }

        #region Get
        public virtual T Get<T>(ApiRoute route)
        {
            try
            {
                var data = GetData(route);
                var content = System.Text.Encoding.Default.GetString(data);

                return JsonConvert.DeserializeObject<T>(content, new JsonSerializerSettings { ContractResolver = new Json.EmptyCollectionContractResolver() });
            }
            catch (Exceptions.AzzysaWebException exc)
            {
                throw exc;
            }
            catch (Exception exc)
            {
                throw Exceptions.AzzysaServerException.Create(exc);
            }
        }
        #endregion

        #region Post

        public virtual T PostForm<T>(ApiRoute route, NameValueCollection pairs)
        {

            try
            {
                using (var client = new System.Net.WebClient())
                {
                    client.Headers.Add(Constants.HttpHeaders.ContentType, MimeTypes.FormData);
                    var result = client.UploadValues(GetApiUrl(route), pairs);
                    return JsonConvert.DeserializeObject<T>(Encoding.Default.GetString(result), new JsonSerializerSettings { ContractResolver = new Json.EmptyCollectionContractResolver() });
                }
            }
            catch (Exceptions.AzzysaWebException exc)
            {
                throw exc;
            }
            catch (Exception exc)
            {
                throw Exceptions.AzzysaServerException.Create(exc);
            }
        }


        public virtual T Post<T>(ApiRoute route, object obj)
        {
            try
            {
                var data = PostData(route, obj);
                var content = System.Text.Encoding.Default.GetString(data);

                return JsonConvert.DeserializeObject<T>(content, new JsonSerializerSettings { ContractResolver = new Json.EmptyCollectionContractResolver() });
            }
            catch (Exceptions.AzzysaWebException exc)
            {
                throw exc;
            }
            catch (Exception exc)
            {
                throw Exceptions.AzzysaServerException.Create(exc);
            }
        }
        #endregion

        #region Core
        private byte[] GetData(ApiRoute route)
        {
            try
            {
                var request = CreateRequest(RequestMethod.GET, GetApiUrl(route));
                return GetResponse(request);
            }
            catch (Exception exc)
            {
                throw ExceptionHandler.BuildException(exc);
            }
        }

        private byte[] PostData(ApiRoute route, object obj, bool compress = false, int? timeout = null)
        {
            var json = JsonConvert.SerializeObject(obj, Formatting.None, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore, ContractResolver = new Json.EmptyCollectionContractResolver() });
            var data = PostData(route, Encoding.Default.GetBytes(json));

            return data;
        }

        private byte[] PostData(ApiRoute route, byte[] data)
        {
            try
            {
                var request = CreateRequest(RequestMethod.POST, GetApiUrl(route));

                using (var s = request.GetRequestStream())
                    s.Write(data, 0, data.Length);

                return GetResponse(request);
            }
            catch (Exception exc)
            {
                throw ExceptionHandler.BuildException(exc);
            }
        }

        private HttpWebRequest CreateRequest(RequestMethod method, Uri url)
        {
            var request = CreateRequest(url);
            request.Method = method.ToString();
            request.Headers[Constants.HttpHeaders.AcceptEncoding] = Constants.ContentEncodings.GZip;
            request.Timeout = Constants.Defaults.HttpWebRequestTimeout;

            if (method == RequestMethod.POST)
                request.ContentType = Constants.MimeTypes.Json;
            request.Accept = Constants.MimeTypes.Json;

            return request;
        }

        public virtual HttpWebRequest CreateRequest(Uri url)
        {
            return (HttpWebRequest)WebRequest.Create(url);
        }

        private byte[] GetResponse(HttpWebRequest request)
        {
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                var contentEncoding = response.Headers[Constants.HttpHeaders.ContentEncoding];

                if (contentEncoding != null && contentEncoding.Equals(Constants.ContentEncodings.GZip))
                {
                    using (var stream = response.GetResponseStream())
                    using (var gs = new GZipStream(stream, CompressionMode.Decompress))
                    using (var ms = new MemoryStream())
                    {
                        gs.CopyTo(ms);
                        return ms.ToArray();
                    }
                }
                else
                {
                    using (var stream = response.GetResponseStream())
                    using (var ms = new MemoryStream())
                    {
                        stream.CopyTo(ms);
                        return ms.ToArray();
                    }
                }
            }
        }

        public Uri GetApiUrl(ApiRoute route)
        {
            return route.GetRouteUrl(url);
        }
        #endregion
    }
}