﻿using System.Collections.Generic;
using System.IO;
using Azzysa.Batch.Web.Models;
using Azzysa.Batch.Web.Models.Dimensions;
using Azzysa.Batch.Web.Models.Documents;
using Azzysa.Batch.Web.Models.Engineering;
using Azzysa.Batch.Web.Models.Files;

namespace Azzysa.Batch.Web
{
    public interface IBatchConnector
    {
        string Test();
        Part GetRelatedFiles(Part part, IList<string> types = null);
        void CheckInFile(Stream fileStream, FcsCheckinRequest request);
        Stream CheckOutFile(FcsCheckoutRequest request);
    }
}
