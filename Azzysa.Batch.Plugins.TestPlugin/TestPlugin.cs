﻿using System;
using System.Collections.Generic;
using Azzysa.Batch.Interfaces;
using Azzysa.Batch.Web;
using Azzysa.Batch.Web.Extensions;
using Azzysa.Batch.Web.Models;
using Azzysa.Batch.Web.Models.Engineering;
using Azzysa.Batch.Web.Models.Files;
using Azzysa.Batch.Web.Models.Parameters;

namespace Azzysa.Batch.Plugins.TestPlugin
{
    public class TestPlugin : IOperationPlugin
    {
        public string Name { get; }
        public string Operation => "test";
        public void Execute(IOperationMessage operationMessage, IBatchConnector connector)
        {
            var properties = operationMessage.GetParameters<RelatedFilesParameters>();

            // test functionality to access Batch Api
            // this connector wil be used to retrieve enovia data (t.b.d.)
            //var response = connector.GetRelatedFiles(
            //    request: new RelatedFilesRequest() {Type = "Part", Name = "000121-00800", Revision = "A01"});

            //using (var stream = System.IO.File.OpenRead(@"D:\TestData\Chris.pdf"))
            //{
            //    connector.CheckInFile(stream, new EnoviaKey("Document", "DOC-0000156", "0"), "generic", "Chris.pdf");
            //}

            var p = connector.GetRelatedFiles(properties.Part, properties.Types);
        }
    }
}
