﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;

namespace Azzysa.Batch.Web.Api
{
    public class UnitOfWork : IUnitOfWork
    {
        private AzzysaBatchContext _dbContext;
        private bool _disposed;

        internal AzzysaBatchContext DbContext => _dbContext ?? (_dbContext = new AzzysaBatchContext());

        ~UnitOfWork()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    DbContext?.Dispose();
                }
                _disposed = true;
            }
        }

        public void SaveChanges()
        {
            DbContext?.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual ICrudRepository<T> Repository<T>() where T : class
        {
            return new CrudRepository<T>(DbContext.Set<T>());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <param name="state"></param>
        public void ChangeEntityState<T>(T entity, EntityState state) where T : class
        {
            DbContext.Entry(entity).State = state;
        }

        public void Update<T>(T entity) where T : class, IIdentifiable
        {
            if (GetEntityState(entity) != EntityState.Detached)
                return;

            T attachedEntity = DbContext.Set<T>().Local.SingleOrDefault(x => x.Id == entity.Id);

            if (attachedEntity != null)
            {
                var attachedEntry = DbContext.Entry(attachedEntity);
                attachedEntry.CurrentValues.SetValues(entity);
            }
            else
            {
                Repository<T>().Update(entity);
                ChangeEntityState(entity, EntityState.Modified);
            }
        }

        public EntityState GetEntityState<T>(T entity) where T : class
        {
            return DbContext.Entry(entity).State;
        }

        public DbTransaction BeginTransaction<T>() where T : DbContext
        {
            if (DbContext.Database.Connection.State != ConnectionState.Open)
            {
                try
                {
                    DbContext.Database.Connection.Open();
                }
                catch (InvalidOperationException)
                {
                    // Suppress exception for cases when connection already opened
                    if (DbContext.Database.Connection.State != ConnectionState.Open)
                    {
                        throw;
                    }
                }
            }
            return DbContext.Database.Connection.BeginTransaction();
        }
    }
}