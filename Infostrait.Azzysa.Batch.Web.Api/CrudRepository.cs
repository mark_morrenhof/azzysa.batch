﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Azzysa.Batch.Web.Api
{
    public class CrudRepository<T> : ICrudRepository<T> where T : class
    {
        private readonly DbSet<T> _dbSet;

        public CrudRepository(DbSet<T> dbSet)
        {
            _dbSet = dbSet;
        }

        public void Create(T entity)
        {
            _dbSet.Add(entity);
        }

        public IQueryable<T> Read()
        {
            return _dbSet.AsNoTracking();
        }

        public IQueryable<T> Read(Expression<Func<T, bool>> predicate)
        {
            return Read().Where(predicate);
        }

        public T SingleOrDefault(Expression<Func<T, bool>> predicate)
        {
            return Read(predicate).FirstOrDefault();
        }

        public bool Any()
        {
            return _dbSet.Any();
        }

        public bool Any(Expression<Func<T, bool>> predicate)
        {
            return _dbSet.Any(predicate);
        }

        public void Update(T entity)
        {
            _dbSet.Attach(entity);
        }

        public void Delete(T entity)
        {
            _dbSet.Remove(entity);
        }
    }
}