﻿using System;
using System.Net;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Azzysa.Batch.Web.Api.Entities;
using Azzysa.Batch.Web.Api;
using Azzysa.Batch.Web.Api.Models;
using Azzysa.Batch.Web.Constants;
using Newtonsoft.Json;
using RestSharp;

namespace Azzysa.Batch.Web.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            UnityConfig.RegisterComponents();
            DatabaseConfig.InitializeDatabases();

            AzzysaBootstrapper.Instance.Start();
            UpdateAgents();
        }

        protected void Application_End(object sender, EventArgs e)
        {
            AzzysaBootstrapper.Instance.Stop(true);
        }

        private void UpdateAgents()
        {
            var uow = new UnitOfWork();
            var agents = uow.Repository<JobAgent>().Read();
            foreach (var jobAgent in agents)
            {
                // todo update agent status
                var client = new RestClient(jobAgent.Target);
                var request = new RestRequest(JobAgentEndpoints.Status, Method.GET) { RequestFormat = DataFormat.Json };

                var response = client.Execute(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var updatedAgent = JsonConvert.DeserializeObject<JobAgent>(response.Content);
                    updatedAgent.Status = JobAgentStatus.Active;
                    updatedAgent.Target = jobAgent.Target;
                    uow.Update(updatedAgent);
                }
                else
                {
                    // log error here
                    jobAgent.Status = JobAgentStatus.Inactive;
                    uow.Update(jobAgent);
                }
            }

            uow.SaveChanges();
        }
    }
}
