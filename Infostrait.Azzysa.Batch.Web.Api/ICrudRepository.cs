﻿using System;
using System.Linq.Expressions;

namespace Azzysa.Batch.Web.Api
{
    public interface ICrudRepository<T>
    {
        /// <summary>
        /// create a entity
        /// </summary>
        /// <param name="entity">the entity to create</param>
        void Create(T entity);

        /// <summary>
        /// Read entities
        /// </summary>
        /// <returns>an iqueryable of entities</returns>
        System.Linq.IQueryable<T> Read();

        /// <summary> 
        /// Read entities with where condition 
        /// </summary> 
        /// <param name="predicate">where condition</param> 
        /// <returns>an iqueryable of entities</returns> 
        System.Linq.IQueryable<T> Read(Expression<Func<T, bool>> predicate);

        /// <summary> 
        /// Read entity with where condition 
        /// </summary> 
        /// <param name="predicate">where condition</param> 
        /// <returns>an entity or null</returns> 
        T SingleOrDefault(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// Find any entity
        /// </summary>
        /// <returns>a boolean</returns>
        bool Any();

        /// <summary> 
        /// Read any entity with condition 
        /// </summary> 
        /// <param name="predicate">the any condition</param> 
        /// <returns>a boolean</returns> 
        bool Any(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// Update entities
        /// </summary>
        /// <param name="entity">the entity to be updated</param>
        void Update(T entity);

        /// <summary>
        /// Delete entities
        /// </summary>
        /// <param name="entity">the entity to delete</param>
        void Delete(T entity);
    }
}