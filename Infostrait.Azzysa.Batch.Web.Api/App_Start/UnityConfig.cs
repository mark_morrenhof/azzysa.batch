﻿using Microsoft.Practices.Unity;
using System.Web.Http;
using Azzysa.Batch.Web.Api;
using Unity.WebApi;

namespace Azzysa.Batch.Web.Api
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            container.RegisterType<IUnitOfWork, UnitOfWork>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}