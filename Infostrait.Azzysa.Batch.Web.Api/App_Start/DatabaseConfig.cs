﻿namespace Azzysa.Batch.Web.Api
{
    public static class DatabaseConfig
    {
        public static void InitializeDatabases()
        {
            using (var context = new AzzysaBatchContext())
            {
                context.Database.Initialize(true);
            }
        }
    }
}