﻿namespace Azzysa.Batch.Web.Api.Entities
{
    public enum JobAgentStatus
    {
        Active,
        Inactive
    }
}