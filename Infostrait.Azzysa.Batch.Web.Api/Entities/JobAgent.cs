﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Azzysa.Batch.Web.Api.Entities
{
    public class JobAgent : IIdentifiable
    {
        private IEnumerable<string> _operations;

        [Key]
        public Guid Id { get; set; }
        public string Target { get; set; }
        public string Port { get; set; }
        public string Version { get; set; }
        public string Site { get; set; }

        [NotMapped]
        public IEnumerable<string> Operations
        {
            get { return _operations; }
            set { _operations = value; }
        }

        public string OperationsString {
            get { return string.Join(",", _operations); }
            set { _operations = value.Split(',');  }
        }

        public JobAgentStatus Status { get; set; }
    }
}