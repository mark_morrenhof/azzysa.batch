﻿using System.Web.Http;
using Microsoft.Practices.Unity;

namespace Azzysa.Batch.Web.Api.Controllers
{
    public class AzzysaBatchControllerBase : ApiController
    {
        [Dependency]
        public IUnitOfWork UnitOfWork { get; set; }
    }
}