﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Azzysa.Batch.Kernel;
using Azzysa.Batch.Web.Api.Entities;
using Azzysa.Batch.Web.Api.Extensions;
using Hangfire;
using Newtonsoft.Json;

namespace Azzysa.Batch.Web.Api.Controllers
{
    public class BatchController : AzzysaBatchControllerBase
    {
        [Route("api/batch/process")]
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Process([FromUri] string objectId)
        {
            var batchId = BatchJob.StartNew(x =>
            {
                var agent = UnitOfWork.Repository<JobAgent>()
                    .Read().ToList()
                    .FirstOrDefault(a => a.Operations.Any(op => op == "test"));

                var parameters = JsonConvert.SerializeObject(new {First = string.Empty, Second = string.Empty });

                x.Enqueue(() => agent.ProcessOperation(new OperationMessage {Operation = "test", Parameters = parameters}));
            });

            return Ok();
        }

        [Route("api/batch/process")]
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Process([FromBody] IEnumerable<BatchMessage> messages)
        {
            var batchId = BatchJob.StartNew(x =>
            {
                string jobId = null;

                foreach (var message in messages)
                {
                    // get appropriate job agent
                    var agent = UnitOfWork.Repository<JobAgent>()
                        .Read().ToList()
                        .FirstOrDefault(ja => ja.Status == JobAgentStatus.Active && ja.Operations.Any(op => op == message.Operation));

                    if (agent == null)
                    {
                        throw new Exception($"No active job agents exisit to process operation {message.Operation}");
                    }

                    var operationParameters = JsonConvert.SerializeObject(message.Properties);

                    if (jobId == null)
                    {
                        jobId = x.Enqueue(() => agent.ProcessOperation(new OperationMessage() { Operation = message.Operation, Parameters = operationParameters}));
                    }
                    else
                    {
                        x.ContinueWith(jobId, () => agent.ProcessOperation(new OperationMessage() { Operation = message.Operation, Parameters = operationParameters }));
                    }
                }
            }, string.Empty);

            return Ok();
        }
    }
}