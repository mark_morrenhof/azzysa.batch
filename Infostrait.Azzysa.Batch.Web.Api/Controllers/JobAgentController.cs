﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using Azzysa.Batch.Web.Api.Entities;

namespace Azzysa.Batch.Web.Api.Controllers
{
    public class JobAgentController : AzzysaBatchControllerBase
    {
        [Route("api/jobagent/register")]
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Register([FromBody] JobAgent agent)
        {
            try
            {
                var existingAgent = UnitOfWork.Repository<JobAgent>().SingleOrDefault(ja => ja.Id == agent.Id);

                agent.Status = JobAgentStatus.Active;
                agent.Target = agent.Target;

                if (existingAgent == null)
                {
                    UnitOfWork.Repository<JobAgent>().Create(agent);
                }
                else
                {
                    UnitOfWork.Update(agent);
                }

                UnitOfWork.SaveChanges();
                return Ok(true);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}