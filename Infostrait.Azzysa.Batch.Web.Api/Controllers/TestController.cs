﻿using System.Web.Http;

namespace Azzysa.Batch.Web.Api.Controllers
{
    public class TestController : AzzysaBatchControllerBase
    {
        [HttpGet]
        [Route("api/test")]
        public IHttpActionResult Test()
        {
            return Ok("api result");
        }
    }
}