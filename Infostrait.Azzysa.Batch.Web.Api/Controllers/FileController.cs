﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Http;
using System.Web.Http.Description;
using Azzysa.Batch.Web.ENOVIA;
using Azzysa.Batch.Web.ENOVIA.Interfaces;
using Azzysa.Batch.Web.Models.Files;
using Azzysa.Batch.Web.Routes;

namespace Azzysa.Batch.Web.Api.Controllers
{
    public class FileController : ApiController
    {
        [HttpPost]
        [Route("api/fcs/ticket/checkout/get")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Get([FromBody] FcsCheckoutRequest fcsTicketRequest)
        {
            try
            {
                var ticket = EnoviaConnector.Instance.File.GetFcsTicketCheckout(fcsTicketRequest);
                return Ok(ticket);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


        [HttpPost]
        [Route("api/fcs/ticket/checkin/get")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Get([FromBody] FcsCheckinRequest fcsTicketRequest)
        {
            try
            {
                var ticket = EnoviaConnector.Instance.File.GetFcsTicketCheckin(fcsTicketRequest);
                return Ok(ticket);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("api/fcs/complete/checkin")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult CompleteCheckin([FromBody] FcsJobReceipt jobReceipt)
        {
            try
            {
                var result = EnoviaConnector.Instance.File.SendJobReceipt(jobReceipt);
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


        [HttpPost]
        [Route("api/get/relatedFiles")]
        public IHttpActionResult GetRelatedFiles([FromBody] RelatedFilesRequest request)
        {
            try
            {
                var result = EnoviaConnector.Instance.File.GetPartWithRelatedFiles(request);

                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("api/get/relatedFiles/{name}/{revision}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetRelatedFiles([FromUri]string name, [FromUri]string revision, [FromBody]IEnumerable<string> types = null)
        {
            try
            {
                var result = EnoviaConnector.Instance.File.GetPartWithRelatedFiles(name, revision, types);

                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}