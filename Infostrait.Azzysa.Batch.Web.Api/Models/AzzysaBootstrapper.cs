﻿using System;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Infostrait.Azzysa.Runtime;

namespace Azzysa.Batch.Web.Api.Models
{
    public class AzzysaBootstrapper : IRegisteredObject
    {
        /// <summary>
        /// Returns a reference to the azzysa bootstrapper
        /// </summary>
        public static readonly AzzysaBootstrapper Instance = new AzzysaBootstrapper();

        /// <summary>
        /// Private member to obtain an exclusive lock required for starting the bootstrapper (ENOVIA runtime)
        /// </summary>
        private readonly object _LockObject = new object();

        // Started variable
        private bool _Started;

        /// <summary>
        /// Initializes the Azzysa Bootstrapper and the required Runtime
        /// </summary>
        public void Start()
        {
            lock (_LockObject)
            {
                // Set/check  started variable
                if (_Started)
                {
                    // Already started
                    return;
                }
                else
                {
                    // Start
                    _Started = true;

                    // Register this object in the WebHosting
                    HostingEnvironment.RegisterObject(this);

                    EnoviaConnector.Register();
                }
            }
        }

        /// <summary>
        /// Returns the <see cref="PoolRuntime"/> object
        /// </summary>
        /// <returns></returns>
        public PoolRuntime GetRuntime()
        {
            lock (_LockObject)
                if (!_Started) { throw new ApplicationException("Bootstrapper not initialized!"); }
            return EnoviaConnector.Runtime;
        }

        public void Stop(bool immediate)
        {
            // Stop runtime
            if (EnoviaConnector.Runtime != null)
                EnoviaConnector.Runtime.Dispose();
            EnoviaConnector.Runtime = null;

            // Unregister this object from the HostingEnvironment
            HostingEnvironment.UnregisterObject(this);

            // Perform garbage collection to cleanup remaining KernelContext objects
            GC.Collect();

        }

        private const string AspNetNamespace = "ASP";

        public static string GetApplicationVersion()
        {
            // Look for web application assembly
            var ctx = HttpContext.Current;
            if (ctx != null)
                return getWebApplicationAssembly(ctx)?.GetName().Version.ToString();
            else
                return getApplicationAssembly()?.GetName().Version.ToString();
        }

        public static Assembly getApplicationAssembly()
        {
            // Try the EntryAssembly, this doesn't work for ASP.NET classic pipeline (untested on integrated)
            var ass = Assembly.GetEntryAssembly();

            // Look for web application assembly
            var ctx = HttpContext.Current;
            if (ctx != null)
                ass = getWebApplicationAssembly(ctx);

            // Fallback to executing assembly
            return ass ?? (Assembly.GetExecutingAssembly());
        }

        private static Assembly getWebApplicationAssembly(HttpContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            object app = context.ApplicationInstance;
            if (app == null) return null;

            Type type = app.GetType();
            while (type != null && type != typeof(object) && type.Namespace == AspNetNamespace)
                type = type.BaseType;

            return type.Assembly;
        }
    }
}