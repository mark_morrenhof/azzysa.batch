﻿using System.Data.Entity;
using Azzysa.Batch.Web.Api.Entities;

namespace Azzysa.Batch.Web.Api
{
    public class AzzysaBatchContext : DbContext
    {

        public AzzysaBatchContext() : base("AzzysaBatchConnection")
        {
        }
        public AzzysaBatchContext(string connectionString) : base(connectionString)
        {
        }

        static AzzysaBatchContext()
        {
            Database.SetInitializer(new Initializer());
        }

        private DbSet<JobAgent> JobAgents { get; set; }
    }

    public class Initializer : IDatabaseInitializer<AzzysaBatchContext>
    {
        public void InitializeDatabase(AzzysaBatchContext context)
        {
            context.Database.CreateIfNotExists();
        }
    }
}