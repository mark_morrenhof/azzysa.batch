﻿using Azzysa.Batch.Kernel;
using Azzysa.Batch.Web.Api.Entities;
using RestSharp;

namespace Azzysa.Batch.Web.Api.Extensions
{
    public static class JobAgentProcessor
    {
        public static void ProcessOperation(this JobAgent agent, OperationMessage operation)
        {
            var client = new RestClient(agent.Target);
            var request = new RestRequest(Method.POST) { RequestFormat = DataFormat.Json };
            request.AddBody(operation);

            var response = client.Execute(request);
        }
    }
}