﻿using System;

namespace Azzysa.Batch.Web.Api
{
    public interface IIdentifiable
    {
        Guid Id { get; set; }
    }
}