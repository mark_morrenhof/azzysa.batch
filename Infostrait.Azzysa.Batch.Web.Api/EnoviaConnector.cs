﻿using Azzysa.Batch.Web.ENOVIA;
using Azzysa.Batch.Web.ENOVIA.Interfaces;
using Infostrait.Azzysa.Runtime;

namespace Azzysa.Batch.Web.Api
{
    public class EnoviaConnector
    {
        public static IEnoviaConnector Instance { get; set; }

        public static PoolRuntime Runtime
        {
            get
            {
                return EnoviaConnection.Runtime;
            }
            set
            {
                EnoviaConnection.Runtime = value;
            }
        }

        public static void Register()
        {
            Instance = EnoviaConnection.Register();
        }
    }
}