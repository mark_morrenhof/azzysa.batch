﻿using System.Data.Common;
using System.Data.Entity;

namespace Azzysa.Batch.Web.Api
{
    public interface IUnitOfWork
    {
        void SaveChanges();

        ICrudRepository<T> Repository<T>() where T : class;

        void Dispose();

        DbTransaction BeginTransaction<T>() where T : DbContext;

        void ChangeEntityState<T>(T entity, EntityState state) where T : class;

        EntityState GetEntityState<T>(T entity) where T : class;

        void Update<T>(T entity) where T : class, IIdentifiable;
    }
}