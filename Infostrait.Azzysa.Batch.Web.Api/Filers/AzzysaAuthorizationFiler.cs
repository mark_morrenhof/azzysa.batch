﻿using Hangfire.Dashboard;

namespace Azzysa.Batch.Web.Api.Filers
{
    public class AzzysaAuthorizationFiler : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            // allow everyone acces to dashboard
            return true;
        }
    }
}