﻿using System.Collections.Generic;
using Azzysa.Batch.Web.Api;
using Azzysa.Batch.Web.Api.Filers;
using Hangfire;
using Hangfire.Dashboard;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace Azzysa.Batch.Web.Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage("HangfireConnection");
            GlobalConfiguration.Configuration.UseBatches();

            app.UseHangfireServer();

            var options = new DashboardOptions()
            {
                Authorization = new [] { new AzzysaAuthorizationFiler() },
            };

            app.UseHangfireDashboard("/dashboard", options);

            GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute() { Attempts = 0, OnAttemptsExceeded = AttemptsExceededAction.Delete });
        }
    }
}
