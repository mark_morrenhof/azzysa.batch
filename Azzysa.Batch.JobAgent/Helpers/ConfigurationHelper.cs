﻿using System;
using System.Configuration;
using System.Net.Mime;

namespace Azzysa.Batch.JobAgent.Helpers
{
    public class ConfigurationHelper
    {
        public static Guid GetAgentId()
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var idSetting = config.AppSettings.Settings["AgentId"];

            Guid id;
            if (idSetting == null)
            {
                // create and return
                id = Guid.NewGuid();
                config.AppSettings.Settings.Add("AgentId", id.ToString());
                config.Save(ConfigurationSaveMode.Modified);
            }
            else if (!Guid.TryParse(idSetting.Value, out id))
            {
                // update and return
                id = Guid.NewGuid();
                config.AppSettings.Settings["AgentId"].Value = id.ToString();
                config.Save(ConfigurationSaveMode.Modified);
            }

            return id;
        }
    }
}