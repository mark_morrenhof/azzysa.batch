﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Azzysa.Batch.JobAgent.Extensions;
using Azzysa.Batch.JobAgent.Helpers;
using Azzysa.Batch.Kernel;
using Azzysa.Batch.Web;
using Azzysa.Batch.Web.Constants;
using Azzysa.Batch.Web.JobAgent;
using Newtonsoft.Json;
using JsonSerializer = RestSharp.Serializers.JsonSerializer;

namespace Azzysa.Batch.JobAgent
{
    public class JobAgent
    {
        public IBatchConnector AzzysaConnector { get; set; }
        public IInternalBatchConnector BatchConnector { get; set; }

        public FcsClient FileClient { get; set; }

        public PluginLoader PluginLoader { get; set; }

        public JobAgentInfo JobAgentInfo { get; set; }

        public JsonSerializer Serializer { get; set; }

        public void Initialize()
        {
            AzzysaConnector = BatchConnection.Register(Properties.Settings.Default.ServerUrl); ;
            BatchConnector = InternalBatchConnection.Register(Properties.Settings.Default.ServerUrl);
            FileClient = new FcsClient();
            PluginLoader = new PluginLoader();
            PluginLoader.Initialize(Properties.Settings.Default.PluginsFolder);
            Serializer = new JsonSerializer();

            JobAgentInfo = new JobAgentInfo
            {
                Id = ConfigurationHelper.GetAgentId(),
                Operations = PluginLoader.Operations,
                Target = Properties.Settings.Default.ListenerAddress.GetUri(Properties.Settings.Default.ListenerPort),
                Site = Properties.Settings.Default.Site,
            };
        }

        public bool Register()
        {
            // handshake with server
            return BatchConnector.Register(JobAgentInfo);
        }

        public void StartListening()
        {
            // handshake Okay - open http listener
            var listener = new HttpListener();
            if (!HttpListener.IsSupported)
            {
                return;
            }

            listener.Prefixes.Add(JobAgentInfo.Target);
            listener.Start();

            while (true)
            {
                var ctx = listener.GetContext();
                if (ctx.Request.Url.LocalPath.Trim('\\', '/').Equals(JobAgentEndpoints.Status, StringComparison.OrdinalIgnoreCase))
                {
                    // if server requested agent's status
                    var agentInfo = new JobAgentInfo
                    {
                        Id = ConfigurationHelper.GetAgentId(),
                        Operations = PluginLoader.Operations,
                        Target = Properties.Settings.Default.ListenerAddress.GetUri(Properties.Settings.Default.ListenerPort),
                        Site = Properties.Settings.Default.Site,
                    };
                    
                    WriteHttpListenerResponse(ctx, agentInfo);
                }
                else
                {
                    // process incoming operation
                    try
                    {
                        ProcessOperation(ctx, PluginLoader, AzzysaConnector);
                        WriteHttpListenerResponse(ctx, null);
                    }
                    catch (Exception e)
                    {
                        WriteHttpListenerError(ctx);
                    }
                }
            }
        }

        public void ProcessOperation(HttpListenerContext context, PluginLoader pluginLoader, IBatchConnector connector)
        {
            using (var reader = new StreamReader(context.Request.InputStream))
            {
                var data = reader.ReadToEnd();

                var operationMessage = JsonConvert.DeserializeObject<OperationMessage>(data);

                var suitablePlugin = pluginLoader.Plugins.FirstOrDefault(p => p.Operation.Contains(operationMessage.Operation));

                suitablePlugin?.Execute(operationMessage, connector);
            }
        }
        private void WriteHttpListenerResponse(HttpListenerContext context, object data)
        {
            
            var json = Serializer.Serialize(data);

            byte[] bytes = Encoding.Default.GetBytes(json);
            context.Response.ContentType = "application/json";
            context.Response.ContentLength64 = bytes.Length;
            context.Response.OutputStream.Write(bytes, 0, bytes.Length);
            context.Response.OutputStream.Close();
        }

        private void WriteHttpListenerError(HttpListenerContext context)
        {
            context.Response.StatusCode = 500;
            context.Response.ContentType = "application/json";
        }
    }
}