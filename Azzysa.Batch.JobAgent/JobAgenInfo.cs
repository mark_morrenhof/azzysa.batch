﻿
using System;
using System.Collections.Generic;
using Azzysa.Batch.Web;

namespace Azzysa.Batch.JobAgent
{
    public class JobAgentInfo : IJobAgentInfo
    {
        public Guid Id { get; set; }
        public IEnumerable<string> Operations { get; set; }
        public string Version { get; set; }
        public string Site { get; set; }
        public string Target { get; set; }
        public string Port { get; set; }
    }
}