﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Azzysa.Batch.Interfaces;
using Azzysa.Batch.JobAgent.Extensions;
using Azzysa.Batch.JobAgent.Helpers;
using Azzysa.Batch.Kernel;
using Azzysa.Batch.Web;
using Azzysa.Batch.Web.JobAgent;
using Newtonsoft.Json;
using RestSharp;
namespace Azzysa.Batch.JobAgent
{
    class Program
    {
        static void Main(string[] args)
        {
            var jobAgent = new JobAgent();

            jobAgent.Initialize();

            // try to register with server every 30 seconds
            while (!jobAgent.Register())
            {
                Thread.Sleep(TimeSpan.FromSeconds(30));
            }

            // open httplistener 
            jobAgent.StartListening();
        }
    }
}
