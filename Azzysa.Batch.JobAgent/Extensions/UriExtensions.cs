﻿using System;

namespace Azzysa.Batch.JobAgent.Extensions
{
    public static class UriExtensions
    {
        public static string GetUri(this string s, int port)
        {
            var builder = new UriBuilder(s) {Port = port};

            return builder.Uri.ToString();
        }
    }
}