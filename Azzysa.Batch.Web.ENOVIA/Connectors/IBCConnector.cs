﻿using System.Collections.Generic;
using System.Linq;
using Azzysa.Batch.Web.ENOVIA.Interfaces;
using Azzysa.Batch.Web.ENOVIA.Routes;
using Azzysa.Batch.Web.Models;
using Azzysa.Batch.Web.Models.Documents;
using Azzysa.Batch.Web.Models.Engineering;
using Azzysa.Batch.Web.Routes;

namespace Azzysa.Batch.Web.ENOVIA.Connectors
{
    internal class IBCConnector : IIBCConnector
    {
        private string url;

        internal IBCConnector(string url)
        {
            this.url = url;

            #region Routes
            //Part Group
            ApiRouteCollection.Add(EnoviaApiRoutes.GetPartGroupWithParts, ApiRoutePathArguments.Name, ApiRoutePathArguments.Revision);
            ApiRouteCollection.Add(EnoviaApiRoutes.GetPartGroupWithParts);
            ApiRouteCollection.Add(EnoviaApiRoutes.GetCADModelWithRelatedComponents, ApiRoutePathArguments.Type, ApiRoutePathArguments.Name, ApiRoutePathArguments.Revision);
            ApiRouteCollection.Add(EnoviaApiRoutes.GetCADModelWithRelatedComponents);
            ApiRouteCollection.Add(EnoviaApiRoutes.UpdatePartGroup);

            //Part
            ApiRouteCollection.Add(EnoviaApiRoutes.GetPart, ApiRoutePathArguments.Name, ApiRoutePathArguments.Revision);
            ApiRouteCollection.Add(EnoviaApiRoutes.GetPart);
            ApiRouteCollection.Add(EnoviaApiRoutes.UpdatePart);

            //Dynamic Attributes
            ApiRouteCollection.Add(EnoviaApiRoutes.GetAvailableDynamicAttributes);
            ApiRouteCollection.Add(EnoviaApiRoutes.GetPartGroupWithDynamicAttributes, ApiRoutePathArguments.Name, ApiRoutePathArguments.Revision);
            ApiRouteCollection.Add(EnoviaApiRoutes.GetPartGroupWithDynamicAttributes);

            //Component
            ApiRouteCollection.Add(EnoviaApiRoutes.GetLatestRevision, ApiRoutePathArguments.Type, ApiRoutePathArguments.Name);
            ApiRouteCollection.Add(EnoviaApiRoutes.GetLatestRevision);
            #endregion
        }

        #region Interface IIBConnector Implementation

        #region Get PartGroup With Parts
        public PartGroup GetPartGroupWithParts(PartGroup pg, IEnoviaCredentials credentials = null) => GetPartGroupWithParts(pg.Name, pg.Revision, credentials);

        public PartGroup GetPartGroupWithParts(string name, string revision, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(url, credentials);
            return client.Get<PartGroup>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetPartGroupWithParts, name, revision));
        }

        public List<PartGroup> GetPartGroupsWithParts(List<PartGroup> list, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(url, credentials);
            return client.Post<List<PartGroup>>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetPartGroupWithParts), list.SelectAsList(pg => pg.ToEnoviaKey()));
        }
        #endregion

        #region Get CADModel With Related Components
        public CADModel GetCADModelWithRelatedComponents(CADModel model, IEnoviaCredentials credentials = null) => GetCADModelWithRelatedComponents(model.Type, model.Name, model.Revision, credentials);

        public CADModel GetCADModelWithRelatedComponents(string type, string name, string revision, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(url, credentials);
            return client.Get<CADModel>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetCADModelWithRelatedComponents, type, name, revision));
        }

        public List<CADModel> GetCADModelsWithRelatedComponents(List<CADModel> list, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(url, credentials);
            return client.Post<List<CADModel>>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetCADModelWithRelatedComponents), list.SelectAsList(m => m.ToEnoviaKey()));
        }
        #endregion

        #region Get Parts
        public Part GetPart(Part part, IEnoviaCredentials credentials = null) => GetPart(part.Name, part.Revision, credentials);

        public Part GetPart(string name, string revision = null, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(url, credentials);
            return client.Get<Part>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetPart, name, revision));
        }

        public List<Part> GetParts(List<Part> list, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(url, credentials);
            return client.Post<List<Part>>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetPart), list.SelectAsList(p => p.ToEnoviaKey()));
        }
        #endregion

        #region Dynamic Attributes
        public DynamicAttributeDefinitionCollection GetAvailableDynamicAttributes(IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(url, credentials);
            return client.Get<DynamicAttributeDefinitionCollection>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetAvailableDynamicAttributes));
        }

        public PartGroup GetPartGroupWithDynamicAttributes(PartGroup pg, IEnoviaCredentials credentials = null) => GetPartGroupWithDynamicAttributes(pg.Name, pg.Revision, credentials);

        public PartGroup GetPartGroupWithDynamicAttributes(string name, string revision, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(url, credentials);
            return client.Get<PartGroup>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetPartGroupWithDynamicAttributes, name, revision));
        }

        public List<PartGroup> GetPartGroupsWithDynamicAttributes(List<PartGroup> list, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(url, credentials);
            return client.Post<List<PartGroup>>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetPartGroupWithDynamicAttributes), list);
        }
        #endregion

        #region Update
        public void Update(Part part, IEnoviaCredentials credentials = null) => Update(new List<Part> { part }, credentials);
        public void Update(PartGroup pg, IEnoviaCredentials credentials = null) => Update(new List<PartGroup> { pg }, credentials);

        public void Update(List<Part> list, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(url, credentials);
            client.Post<BackgroundTask>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.UpdatePart), list);
        }

        public void Update(List<PartGroup> list, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(url, credentials);
            client.Post<BackgroundTask>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.UpdatePartGroup), list);
        }
        #endregion

        #region Base Component
        public BaseComponent GetLatestRevision(BaseComponent component, IEnoviaCredentials credentials = null) => GetLatestRevision(component.Type, component.Name, credentials);

        public BaseComponent GetLatestRevision(string type, string name, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(url, credentials);
            return client.Get<BaseComponent>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetLatestRevision, type, name));
        }

        public List<BaseComponent> GetLatestRevisions(List<BaseComponent> list, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(url, credentials);
            return client.Post<List<BaseComponent>>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetLatestRevision), list);
        }
        #endregion

        #endregion
    }
}
