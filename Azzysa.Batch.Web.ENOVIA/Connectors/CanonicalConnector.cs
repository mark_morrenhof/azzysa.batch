﻿using System.Collections.Generic;
using Azzysa.Batch.Web.ENOVIA.Interfaces;
using Azzysa.Batch.Web.ENOVIA.Models.Canonical;
using Azzysa.Batch.Web.ENOVIA.Routes;
using Azzysa.Batch.Web.Routes;
using Vanderlande.Interfaces.CanonicalData;

namespace Azzysa.Batch.Web.ENOVIA.Connectors
{
    public class CanonicalConnector : ICanonicalConnector
    {
        private string url;

        internal CanonicalConnector(string url)
        {
            this.url = url;

            #region Routes
            //Canonical Data
            ApiRouteCollection.Add(EnoviaApiRoutes.GetCanonicalData, ApiRoutePathArguments.Type, ApiRoutePathArguments.Name, ApiRoutePathArguments.Revision);
            #endregion
        }

        #region Interface IEnoviaCanonicalConnector Implementation
        public List<Models.Canonical.BusinessObject> GetCanonicalMessageData(string type, string name, string revision, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(url, credentials);
            return client.Get<List<Models.Canonical.BusinessObject>>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetCanonicalData, type, name, revision));
        }

        public DataType GetCanonicalMessage(string type, string name, string revision, IEnoviaCredentials credentials = null)
        {
            // Get canonical data
            var client = new WebClient(url, credentials);
            var data = client.Get<List<Models.Canonical.BusinessObject>>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetCanonicalData, type, name, revision));

            // Iterate through root objects and create canonical type accordinly
            var message = new DataType();
            foreach (var businessObject in data)
            {
                switch (businessObject.Type)
                {
                    case "Part":
                        // Create Part data type
                        message.CreateItemType(businessObject, data);
                        break;
                    default:
                        // Do not process this object type (probably queried because it is referenced by other objects (like responsible engineers)
                        break;
                }
            }
            return message;
        }
        #endregion
    }
}