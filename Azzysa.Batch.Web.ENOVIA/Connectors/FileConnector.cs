﻿using System.Collections.Generic;
using System.Collections.Specialized;
using Azzysa.Batch.Web.ENOVIA.Interfaces;
using Azzysa.Batch.Web.ENOVIA.Routes;
using Azzysa.Batch.Web.Models.Engineering;
using Azzysa.Batch.Web.Models.Files;
using Azzysa.Batch.Web.Routes;

namespace Azzysa.Batch.Web.ENOVIA.Connectors
{
    public class FileConnector : IFileConnector
    {
        private string _url;

        internal FileConnector(string url)
        {
            _url = url;

            #region Routes
            // Ticket
            ApiRouteCollection.Add(EnoviaApiRoutes.GetFcsTicketCheckout);
            ApiRouteCollection.Add(EnoviaApiRoutes.GetFcsTicketCheckin);
            ApiRouteCollection.Add(EnoviaApiRoutes.CompleteCheckin);
            ApiRouteCollection.Add(EnoviaApiRoutes.GetRelatedFiles);
            #endregion
        }

        public FcsTicket GetFcsTicketCheckout(FcsCheckoutRequest ticketRequest, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(_url, credentials);
            return client.Post<FcsTicket>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetFcsTicketCheckout), ticketRequest);
        }

        public FcsTicket GetFcsTicketCheckin(FcsCheckinRequest ticketRequest, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(_url, credentials);
            return client.Post<FcsTicket>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetFcsTicketCheckin), ticketRequest);
        }

        public string SendJobReceipt(FcsJobReceipt receipt, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(_url, credentials);
            return client.Post<string>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.CompleteCheckin), receipt);
        }

        #region Part With Rlated Files

        public Part GetPartWithRelatedFiles(Part part, IEnumerable<string> types, IEnoviaCredentials credentials = null) => GetPartWithRelatedFiles(new RelatedFilesRequest() {Name = part.Name, Revision = part.Revision, TypeNames = types}, credentials);

        public Part GetPartWithRelatedFiles(string name, string revision, IEnumerable<string> types, IEnoviaCredentials credentials = null) => GetPartWithRelatedFiles(new RelatedFilesRequest() { Name = name, Revision = revision, TypeNames = types }, credentials);

        public Part GetPartWithRelatedFiles(RelatedFilesRequest request, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(_url, credentials);
            return client.Post<Part>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetRelatedFiles), request);
        }

        #endregion
    }
}