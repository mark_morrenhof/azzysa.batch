﻿using Azzysa.Batch.Web.ENOVIA.Interfaces;

namespace Azzysa.Batch.Web.ENOVIA.Connectors
{
    internal class EnoviaConnector : IEnoviaConnector
    {
        public IIBCConnector IBC { get; internal set;  }
        public ICanonicalConnector Canonical { get; internal set; }
        public IFileConnector File { get; }

        public EnoviaConnector(string url)
        {
            IBC = new IBCConnector(url);
            Canonical = new CanonicalConnector(url);
            File = new FileConnector(url);
        }
    }
}
