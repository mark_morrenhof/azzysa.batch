﻿namespace Azzysa.Batch.Web.ENOVIA.Interfaces
{
    public interface IEnoviaConnector
    {
        IIBCConnector IBC { get; }
        ICanonicalConnector Canonical { get; }
        IFileConnector File { get; }
   }
}
