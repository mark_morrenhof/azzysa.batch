﻿using System.Collections.Generic;
using Vanderlande.Interfaces.CanonicalData;

namespace Azzysa.Batch.Web.ENOVIA.Interfaces
{
    public interface ICanonicalConnector
    {

        /// <summary>
        /// Returns the required data for one or more object(s) in order to construct a canonical data message
        /// </summary>
        /// <param name="type">ENOVIA type name pattern of the object to retrieve</param>
        /// <param name="name">Name pattern of the objects to retrieve</param>
        /// <param name="revision">Revision pattern of the objects to retrieve</param>
        /// <returns></returns>
        List<Models.Canonical.BusinessObject> GetCanonicalMessageData(string type, string name, string revision, IEnoviaCredentials credentials = null);

        /// <summary>
        /// Returns the canonical message for the specified objects
        /// </summary>
        /// <param name="type">ENOVIA type name pattern of the object to retrieve</param>
        /// <param name="name">Name pattern of the objects to retrieve</param>
        /// <param name="revision">Revision pattern of the objects to retrieve</param>
        /// <returns></returns>
        DataType GetCanonicalMessage(string type, string name, string revision, IEnoviaCredentials credentials = null);

    }
}
