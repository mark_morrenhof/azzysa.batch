﻿using System.Collections.Generic;
using Azzysa.Batch.Web.Models.Engineering;
using Azzysa.Batch.Web.Models.Files;

namespace Azzysa.Batch.Web.ENOVIA.Interfaces
{
    public interface IFileConnector
    {
        FcsTicket GetFcsTicketCheckout(FcsCheckoutRequest ticketRequest, IEnoviaCredentials credentials = null);

        FcsTicket GetFcsTicketCheckin(FcsCheckinRequest ticketRequest, IEnoviaCredentials credentials = null);

        string SendJobReceipt(FcsJobReceipt receipt, IEnoviaCredentials credentials = null);

        #region Part With Related Files

        Part GetPartWithRelatedFiles(Part part, IEnumerable<string> types, IEnoviaCredentials credentials = null);
        Part GetPartWithRelatedFiles(string name, string revision, IEnumerable<string> types, IEnoviaCredentials credentials = null);
        Part GetPartWithRelatedFiles(RelatedFilesRequest request, IEnoviaCredentials credentials = null);

        #endregion
    }
}