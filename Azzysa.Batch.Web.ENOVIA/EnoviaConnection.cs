﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Azzysa.Batch.Web.ENOVIA.Connectors;
using Azzysa.Batch.Web.ENOVIA.Interfaces;
using Azzysa.Batch.Web.Exceptions;
using Infostrait.Azzysa.Runtime;
using Infostrait.Azzysa.Runtime.ENOVIA;


namespace Azzysa.Batch.Web.ENOVIA
{
    public class EnoviaConnection
    {
        #region Constants
        private const string keyConnectionString    = "Azzysa.Runtime.SettingsProvider.ConnectionString";
        private const string keyServerName          = "Azzysa.Environment.ServerName";
        private const string keySiteName            = "Azzysa.Environment.Site";
        private const string keyRootPath            = "Azzysa.Runtime.RootPath";
        private const string keyUserName            = "Azzysa.ConnectionPool.UserName";
        private const string keyVault               = "Azzysa.ConnectionPool.Vault";
        private const string keyPassword            = "Azzysa.ConnectionPool.Password";

        #endregion

        public static PoolRuntime Runtime { get; set; }
        public static IEnoviaCredentials DefaultCredentials { get; set; }

        public static IEnoviaConnector Register(string url = null, IEnoviaCredentials credentials = null)
        {
            var missing = new List<string>();
            var connectionString = url;

            if (url == null)
            {
                if (!ConfigurationManager.AppSettings.AllKeys.Contains(keyConnectionString))
                    missing.Add(keyConnectionString);
                else
                    connectionString = ConfigurationManager.AppSettings[keyConnectionString];
            }
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(keyServerName))
                missing.Add(keyServerName);
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(keySiteName))
                missing.Add(keySiteName);
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(keyRootPath))
                missing.Add(keyRootPath);

            if (credentials == null)
            {
                if (!ConfigurationManager.AppSettings.AllKeys.Contains(keyUserName))
                    missing.Add(keyUserName);
                if (!ConfigurationManager.AppSettings.AllKeys.Contains(keyPassword))
                    missing.Add(keyPassword);
                if (!ConfigurationManager.AppSettings.AllKeys.Contains(keyVault))
                    missing.Add(keyVault);
            }

            if (missing.Count > 0)
                throw new MissingConfigurationException(missing);

            return Register(connectionString,
                    ConfigurationManager.AppSettings[keyServerName],
                    ConfigurationManager.AppSettings[keySiteName],
                    ConfigurationManager.AppSettings[keyRootPath],
                    credentials != null ? credentials.UserName : ConfigurationManager.AppSettings[keyUserName],
                    credentials != null ? credentials.Password : ConfigurationManager.AppSettings[keyPassword],
                    credentials != null ? credentials.Vault : ConfigurationManager.AppSettings[keyVault]);
        }

        public static IEnoviaConnector Register(string connectionString, string serverName, string siteName, string rootPath, string userName, string password, string vault) => Register(connectionString, serverName, siteName, rootPath, new EnoviaCredentials() { UserName = userName, Password = password, Vault = vault });

        public static IEnoviaConnector Register(string connectionString, string serverName, string siteName, string rootPath, IEnoviaCredentials credentials)
        {
            #region Initialize
            //Initialize azzysa runtime at first
            Runtime = new ENOVIARuntime(connectionString, serverName, siteName, rootPath);
            // Set login credentials
            Runtime.UserName = credentials.UserName;
            Runtime.Password = credentials.Password;
            Runtime.Vault = credentials.Vault;

            // Initialize the runtime
            Runtime.Initialize();
            #endregion

            DefaultCredentials = credentials;

            return new EnoviaConnector(connectionString);
        }
    }
}
