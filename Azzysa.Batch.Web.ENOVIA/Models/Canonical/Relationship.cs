﻿using System.Collections.Generic;

namespace Azzysa.Batch.Web.ENOVIA.Models.Canonical
{
    public class Relationship
    {
        public BusinessObject FromObject { get; set; }
        public BusinessObject ToObject { get; set; }
        public string RelationshipType { get; set; }
        public string RelationShipId { get; set; }
        public string direction { get; set; }
        public List<BusinessObjectAttribute> Attributes { get; set; }

        public Relationship()
        {
            this.Attributes = new List<BusinessObjectAttribute>();
        }

        public override string ToString()
        {
            return $"{RelationshipType} | {FromObject?.Type} {FromObject?.Name} {FromObject?.Revision} | {ToObject?.Type} {ToObject?.Name} {ToObject?.Revision}";
        }
    }
}
