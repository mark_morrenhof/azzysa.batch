﻿using System;
using System.Collections.Generic;
using Vanderlande.Interfaces.CanonicalData;

namespace Azzysa.Batch.Web.ENOVIA.Models.Canonical
{
    public static class DataTypeExtensions
    {

        /// <summary>
        /// Extensions method creates a new <see cref="Vanderlande.Interfaces.CanonicalData.ItemType"/> object according to the <see cref="BusinessObject"/> data
        /// </summary>
        /// <param name="message"><see cref="Vanderlande.Interfaces.CanonicalData.DataType"/> object containing the complete message to add the object to</param>
        /// <param name="businessObject"><see cref="BusinessObject"/> holding the data retrieved from ENOVIA</param>
        public static void CreateItemType(this DataType message, BusinessObject businessObject, List<BusinessObject> businessObjectCollection)
        {
            // Create new item root type
            var rootItem = CreateItemTypeObject(businessObject, false, businessObjectCollection);

            // Add referenced Parts as well
            businessObject.ConnectedObjects.FindAll(r => r.RelationshipType.Equals("EBOM", System.StringComparison.InvariantCultureIgnoreCase))
                .ForEach((relationship) => CreateItemBoMLine(relationship, rootItem, message, businessObjectCollection));

            // Add to message collection
            message.Items = new Vanderlande.Interfaces.CanonicalData.ItemCollection();
            message.Items.Add(rootItem);

        }

        /// <summary>
        /// Helper method used by the <see cref="CreateItemType(DataType, BusinessObject)"/> method that actually creates the <see cref="ItemType"/> object
        /// </summary>
        /// <param name="businessObject"><see cref="BusinessObject"/> holding the data retrieved from ENOVIA</param>
        /// <param name="keyOnly">Boolean value indicating whether to add key values only to the return object or all attributes</param>
        /// <returns></returns>
        private static ItemType CreateItemTypeObject(BusinessObject businessObject, bool keyOnly, List<BusinessObject> businessObjectCollection)
        {
            // Create new object
            var itemType = new ItemType();

            // Always add keyonly fields
            itemType.Key.ItemNumber = businessObject.Name;
            itemType.Key.Revision = businessObject.Revision;

            // Check whether to add non-key fields as well
            if (!keyOnly)
            {
                // Set TechnicalState
                switch (businessObject.State)
                {
                    case "Requested":
                        itemType.TechnicalState = TechnicalStateType.Draft;
                        break;
                    case "Draft":
                        itemType.TechnicalState = TechnicalStateType.Draft;
                        break;
                    case "For Review":
                        itemType.TechnicalState = TechnicalStateType.Draft;
                        break;
                    case "Reviewed":
                        itemType.TechnicalState = TechnicalStateType.Draft;
                        break;
                    case "Conditional Use":
                        itemType.TechnicalState = TechnicalStateType.Released;
                        break;
                    case "Unconditional Use":
                        itemType.TechnicalState = TechnicalStateType.Released;
                        break;
                    case "Phase Out":
                        itemType.TechnicalState = TechnicalStateType.Released;
                        break;
                    case "Out Of Date":
                        itemType.TechnicalState = TechnicalStateType.Released;
                        break;
                    case "Superseded":
                        itemType.TechnicalState = TechnicalStateType.Released;
                        break;
                    case "End Of Life":
                        itemType.TechnicalState = TechnicalStateType.Obsolete;
                        break;
                    default:
                        throw new ApplicationException($"Unknown state {businessObject.State} encountered when converting to canonical type for {businessObject.Type}, {businessObject.Name}, {businessObject.Revision}");
                }

                // Set TechnicalPhaseType
                if (bool.Parse(businessObject.GetAttribute("VI On Hold").Value))
                {
                    // On hold
                    itemType.Phase = TechnicalPhaseType.Onhold;
                }
                else {
                    // Set TechnicalPhaseType
                    switch (businessObject.State)
                    {
                        case "Requested":
                            itemType.Phase = TechnicalPhaseType.Requested;
                            break;
                        case "Draft":
                            itemType.Phase = TechnicalPhaseType.Requested;
                            break;
                        case "For Review":
                            itemType.Phase = TechnicalPhaseType.Forreview;
                            break;
                        case "Reviewed":
                            itemType.Phase = TechnicalPhaseType.Reviewed;
                            break;
                        case "Conditional Use":
                            itemType.Phase = TechnicalPhaseType.Conditionaluse;
                            break;
                        case "Unconditional Use":
                            itemType.Phase = TechnicalPhaseType.Unconditionaluse;
                            break;
                        case "Phase Out":
                            itemType.Phase = TechnicalPhaseType.Restricteduse;
                            break;
                        case "Out Of Date":
                            itemType.Phase = TechnicalPhaseType.Onhold;
                            break;
                        case "Superseded":
                            itemType.Phase = TechnicalPhaseType.Superseded;
                            break;
                        case "End Of Life":
                            itemType.Phase = TechnicalPhaseType.Endoflife;
                            break;
                        default:
                            throw new ApplicationException($"Unknown state {businessObject.State} encountered when converting to canonical type for {businessObject.Type}, {businessObject.Name}, {businessObject.Revision}");
                    }
                }

                // Description
                itemType.Description = businessObject.Description;

                // Description 1
                itemType.ShortDescription1 = businessObject.GetAttribute("VI Description 1").Value;
                if (string.IsNullOrEmpty(itemType.ShortDescription1))
                {
                    if (string.IsNullOrEmpty(itemType.Description))
                    {
                        itemType.ShortDescription1 = "NO DESCRIPTION AVAILABLE";
                    }
                    else
                    {
                        if (itemType.Description.Length > 30)
                        {
                            itemType.ShortDescription1 = $"{itemType.Description.Substring(0, 27)}...";
                        }
                        else
                        {
                            itemType.ShortDescription1 = itemType.Description;
                        }
                    }
                }

                // Description 2
                itemType.ShortDescription2 = businessObject.GetAttribute("VI Description 2").Value;

                // Unit of Measure
                switch (businessObject.GetAttribute("Unit of Measure").Value)
                {
                    case "GA (gallon)":
                    case "CM (centimeter)":
                    case "CM^2 (square centimeter)":
                    case "CM^3 (cubic centimeter)":
                    case "IN^3 (cubic inch)":
                    case "FT^3 (cubic feet)":
                    case "T (ton)":
                    case "ML (milliliter)":
                    case "CL (centiliter)":
                    case "PT (pint)":
                    case "QT (quart)":
                    case "PPM (parts per million)":
                    case "MG (milligram)":
                    case "CG (centigram)":
                    case "G (gram)":
                    case "MM^2 (square millimeter)":
                    case "M^3 (cubic meter)":
                    case "MM^3 (cubic millimeter)":
                    case "WEIGHTLESS":
                    case "YD (yard)":
                    case "Batch":
                    case "% (percent)":
                    case "LB (pound)":
                        throw new ApplicationException($"Unit of Measure type {businessObject.GetAttribute("Unit of Measure").Value} not supported when creating ItemType for {businessObject.Type}, {businessObject.Name}, {businessObject.Revision}");
                    case "IN (inch)":
                        itemType.UnitOfMeasure = UnitOfMeasureType.Inch;
                        break;
                    case "FT (feet)":
                        itemType.UnitOfMeasure = UnitOfMeasureType.Foot;
                        break;
                    case "EA (each)":
                        itemType.UnitOfMeasure = UnitOfMeasureType.Each;
                        break;
                    case "FT^2 (square feet)":
                        itemType.UnitOfMeasure = UnitOfMeasureType.Squarefoot;
                        break;
                    case "IN^2 (square inch)":
                        itemType.UnitOfMeasure = UnitOfMeasureType.Squareinch;
                        break;
                    case "LITER (liter)":
                        itemType.UnitOfMeasure = UnitOfMeasureType.Liter;
                        break;
                    case "M (meter)":
                        itemType.UnitOfMeasure = UnitOfMeasureType.Meter;
                        break;
                    case "M^2 (square meter)":
                        itemType.UnitOfMeasure = UnitOfMeasureType.Squaremeter;
                        break;
                    case "MM (millimeter)":
                        itemType.UnitOfMeasure = UnitOfMeasureType.Millimeter;
                        break;
                    case "KG (kilogram)":
                        itemType.UnitOfMeasure = UnitOfMeasureType.Kilogram;
                        break;
                    case "OZ (ounce)":
                        itemType.UnitOfMeasure = UnitOfMeasureType.Ounce;
                        break;
                    default:
                        throw new ApplicationException($"Unit of Measure type {businessObject.GetAttribute("Unit of Measure").Value} not expected when creating ItemType for {businessObject.Type}, {businessObject.Name}, {businessObject.Revision}");
                }

                // Base material
                if (businessObject.ConnectedObjects.Exists(o => o.RelationshipType.Equals("VI Base Material", StringComparison.InvariantCulture)))
                {
                    var baseMaterial = businessObject.ConnectedObjects.Find(o => o.RelationshipType.Equals("VI Base Material", StringComparison.InvariantCulture));
                    itemType.BaseMaterialItem = CreateItemTypeObject(baseMaterial.ToObject, true, businessObjectCollection);
                }

                // Responsible engineer
                var employeeObject = businessObjectCollection.Find(o => o.Name.Equals(businessObject.GetAttribute("Responsible Design Engineer").Value, StringComparison.InvariantCulture) &&
                o.Type.Equals("Person", StringComparison.InvariantCulture));
                if (employeeObject != null)
                {
                    itemType.ResponsibleEngineer = CreateEmployeeType(employeeObject, true);
                }
                else
                {
                    throw new ApplicationException($"Unknown responsible engineer {businessObject.GetAttribute("Responsible Design Engineer").Value} encountered when converting to canonical type for {businessObject.Type}, {businessObject.Name}, {businessObject.Revision}");
                }

                // Responsible department
                var departmentObject = businessObject.ConnectedObjects.Find(p => p.RelationshipType.Equals("Design Responsibility", StringComparison.InvariantCulture))?.FromObject;
                if (departmentObject != null)
                {
                    itemType.ResponsibleDepartment = new DepartmentType();
                    itemType.ResponsibleDepartment.Key.ID = departmentObject.GetAttribute("Organization ID").Value;
                    itemType.ResponsibleDepartment.Name = departmentObject.GetAttribute("Organization Name").Value;
                }
                else
                {
                    throw new ApplicationException($"Responsible department not found for {businessObject.Type}, {businessObject.Name}, {businessObject.Revision} when converting to canonical type");
                }

                // VI Preferred (VI Keep Up To Date)
                itemType.Preferred = bool.Parse(businessObject.GetAttribute("VI Keep Up To Date").Value);

                // Make / Buy (VI Buy)
                itemType.Buy = bool.Parse(businessObject.GetAttribute("VI Buy").Value);

                // VI Engineered (VI VI Engineered)
                itemType.VIEngineered = bool.Parse(businessObject.GetAttribute("VI VI Engineered").Value);

                // TODO: CN_EXCLUDE_FROM_BOM

                // Spare Part
                if ((bool)businessObject.GetAttribute("Spare Part").Value?.Equals("Yes", StringComparison.InvariantCulture))
                {
                    itemType.SparePart = true;
                } else  if ((bool)businessObject.GetAttribute("Spare Part").Value?.Equals("No", StringComparison.InvariantCulture))
                {
                    itemType.SparePart = false;
                }

                // SparePartCategory
                switch (businessObject.GetAttribute("VI Spare Part Type").Value)
                {
                    case "C - Consumable":
                        itemType.SparePartCategory = SparePartCategoryType.Consumable;
                        break;
                    case "D - Durable":
                        itemType.SparePartCategory = SparePartCategoryType.Durable;
                        break;
                    case "DR - Durable Repairabl":
                        itemType.SparePartCategory = SparePartCategoryType.Durablerepairable;
                        break;
                    case "N - No":
                        itemType.SparePartCategory = SparePartCategoryType.No;
                        break;
                    case "R - Repairable":
                        itemType.SparePartCategory = SparePartCategoryType.Repairable;
                        break;
                    case "T - Tool":
                        itemType.SparePartCategory = SparePartCategoryType.Tool;
                        break;
                    case "W - Wear":
                        itemType.SparePartCategory = SparePartCategoryType.Wear;
                        break;
                    case "WR - Wear Repairable":
                        itemType.SparePartCategory = SparePartCategoryType.Wearrepairable;
                        break;
                    default:
                        throw new ApplicationException($"Unknown spare part type {businessObject.GetAttribute("VI Spare Part Type").Value} encountered when converting to canonical type for {businessObject.Type}, {businessObject.Name}, {businessObject.Revision} when converting to canonical type");
                }

                // Set colouring freedom (depending on whether object has a relationship with a treatment through the 'VI TRC Epoxy Coating' relationship having a color set at the link
                var epoxyCoatingRel = businessObject.ConnectedObjects.Find(p => p.RelationshipType.Equals("VI TRC Epoxy Coating", StringComparison.InvariantCulture)
                    && p.Attributes.Exists(a => a.Name.Equals("VI TRC Treatment Colour", StringComparison.InvariantCulture) && !string.IsNullOrWhiteSpace(a.Value)));
                if (epoxyCoatingRel == null)
                {
                    // Free
                    itemType.ColouringFreedom = ColouringFreedomType.Free;
                } else
                {
                    // Fixed (not null)
                    itemType.ColouringFreedom = ColouringFreedomType.Fixed;

                    // Get colour
                    var colour = epoxyCoatingRel.Attributes.Find(p => p.Name.Equals("VI TRC Treatment Colour", StringComparison.InvariantCulture)).Value.Trim();
                    if (colour.Contains(" "))
                        colour = colour.Substring(0, colour.IndexOf(" "));
                    itemType.Colour = (ColourType)Enum.Parse(typeof(ColourType), colour);
                }

                // Set classification path 1 to 5
                var classificationPath = businessObject.ConnectedObjects.Find(p => p.RelationshipType.Equals("Classified Item", StringComparison.InvariantCulture))?
                    .FromObject?.Attributes.Find(p => p.Name.Equals("VI Classification Path", StringComparison.InvariantCulture))?.Value;
                if (!string.IsNullOrWhiteSpace(classificationPath))
                {
                    var path = new List<string>(classificationPath.Split(new char[] { '{', '}' }, StringSplitOptions.RemoveEmptyEntries));
                    foreach (var s in path)
                    {
                        // Always use the second value (so 2nd, 4th, 6th, 8th or 10th) as the format is {{id} {name}}{{id}{name}}
                        if ((path.IndexOf(s) % 2) == 1)
                        {
                            // Determine current classification level
                            switch ((path.IndexOf(s) + 1) / 2)
                            {
                                case 1:
                                    itemType.ClassificationLevel1 = s;
                                    break;
                                case 2:
                                    itemType.ClassificationLevel2 = s;
                                    break;
                                case 3:
                                    itemType.ClassificationLevel3 = s;
                                    break;
                                case 4:
                                    itemType.ClassificationLevel4 = s;
                                    break;
                                case 5:
                                    itemType.ClassificationLevel5 = s;
                                    break;
                                default:
                                    throw new ApplicationException($"Invalid classification path {classificationPath} for {businessObject.Type}, {businessObject.Name}, {businessObject.Revision} when converting to canonical type (too many levels)");
                            }
                        }
                    }
                    
                } else
                {
                    // Unable to set classification path 1 to 5 since the classification path value is missing
                }

                // Dynamic attributes (technical attributes)
                foreach (var attribute in businessObject.Attributes)
                {
                    if (businessObject.DynamicAttributes.Contains(attribute.Name))
                    {
                        // Technical/dynamic attribute
                        if (attribute.Name.Equals("VI_GrossMatMeasure", StringComparison.InvariantCulture))
                        {
                            itemType.GrossMaterialMeasure = double.Parse(attribute.Value, System.Globalization.CultureInfo.InvariantCulture);
                        }
                        if (attribute.Name.Equals("VI_Mass", StringComparison.InvariantCulture))
                        {
                            switch(attribute.Unit)
                            {
                                case "POUND":
                                case "SLUG":
                                case "SLUG12":
                                case "TONNE":
                                case "KGFxS2_MM":
                                case "LBFxS2_FT":
                                case "LBFxS2_IN":
                                case "MG":
                                case "KGFxS2_CM":
                                case "GRAVITY_TON":
                                case "HECTOGRAM":
                                    throw new ApplicationException($"Unit of Measure type {attribute.Unit} for ItemType.WeightUoM not supported when creating ItemType for {businessObject.Type}, {businessObject.Name}, {businessObject.Revision}");
                                case "GRAM":
                                    itemType.WeightUoM = UnitOfMeasureType.Gram;
                                    break;
                                case "KGFxS2_M":
                                    itemType.WeightUoM = UnitOfMeasureType.KilogramSquareMeter;
                                    break;
                                case "KILOGRAM":
                                    itemType.WeightUoM = UnitOfMeasureType.Kilogram;
                                    break;
                                case "OUNCE":
                                    itemType.WeightUoM = UnitOfMeasureType.Ounce;
                                    break;
                                default:
                                    throw new ApplicationException($"Unit of Measure type {attribute.Unit} for ItemType.WeightUoM not expected when creating ItemType for {businessObject.Type}, {businessObject.Name}, {businessObject.Revision}");
                            }
                        }

                        // Create classification attribute
                        var classificationAttributeType = new ClassificationAttributeType();
                        classificationAttributeType.Name = attribute.Name;
                        classificationAttributeType.NormalisedUnitOfMeasure = attribute.Unit;
                        classificationAttributeType.UnitOfMeasure = attribute.InputUnit;
                        switch (attribute.DataType)
                        {
                            case "real":
                                classificationAttributeType.Value = double.Parse(attribute.InputValue, System.Globalization.CultureInfo.InvariantCulture);
                                classificationAttributeType.NormalisedValue = double.Parse(attribute.Value, System.Globalization.CultureInfo.InvariantCulture);
                                break;
                            case "string":
                                classificationAttributeType.Value = attribute.InputValue;
                                break;
                            case "boolean":
                                classificationAttributeType.Value = bool.Parse(attribute.InputValue);
                                break;
                            case "timestamp":
                                if (!string.IsNullOrWhiteSpace(attribute.Value))
                                {
                                    // Parse date-string
                                    classificationAttributeType.Value = DateTime.ParseExact(attribute.Value,
                                        "M/d/yyyy h:mm:ss tt",
                                        System.Globalization.CultureInfo.InvariantCulture);
                                }
                                if (!string.IsNullOrWhiteSpace(attribute.InputValue))
                                {
                                    // Parse date-string
                                    classificationAttributeType.Value = DateTime.ParseExact(attribute.InputValue,
                                        "M/d/yyyy h:mm:ss tt",
                                        System.Globalization.CultureInfo.InvariantCulture);
                                }
                                break;
                            case "integer":
                                classificationAttributeType.Value = int.Parse(attribute.InputValue, System.Globalization.CultureInfo.InvariantCulture);
                                classificationAttributeType.NormalisedValue = int.Parse(attribute.Value, System.Globalization.CultureInfo.InvariantCulture);
                                break;
                            default:
                                throw new ApplicationException($"Data type {attribute.DataType} for classification attribute {attribute.Name} not supported when creating ItemType for {businessObject.Type}, {businessObject.Name}, {businessObject.Revision}");
                        }
                        if (itemType.ClassificationAttributes == null)
                        {
                            itemType.ClassificationAttributes = new ClassificationAttributeCollection();
                        }
                        itemType.ClassificationAttributes.Add(classificationAttributeType);
                    }
                }

                // TODO: CN_OEM_NUMBER
                // TODO: CN_OEM

            }

            // Return the created object
            return itemType;
        }

        private static void CreateItemBoMLine(Relationship eBoMRelationship, ItemType parent, DataType message, List<BusinessObject> businessObjectCollection)
        {
            // Create new BoM line
            var bomLine = new Vanderlande.Interfaces.CanonicalData.ItemTypeBOMLine();
            bomLine.Item = CreateItemTypeObject(eBoMRelationship.ToObject, true, businessObjectCollection);
            bomLine.Quantity = eBoMRelationship.GetAttribute("Quantity").Value;
            bomLine.Measure = double.Parse(eBoMRelationship.GetAttribute("VI Measure").Value, System.Globalization.CultureInfo.InvariantCulture);
            bomLine.PositionNumber = eBoMRelationship.GetAttribute("Find Number").Value;
            bomLine.SparePart = double.Parse(eBoMRelationship.GetAttribute("VI Spare Part Criticality").Value, System.Globalization.CultureInfo.InvariantCulture) > 0;
            
            // Add to parent
            if (parent.BOM == null)
            {
                parent.BOM = new ItemBomCollection();
            }
            parent.BOM.Add(bomLine);
        }

        private static EmployeeType CreateEmployeeType(BusinessObject employeeObject, bool keyOnly)
        {
            var employeeType = new Vanderlande.Interfaces.CanonicalData.EmployeeType();
            employeeType.Key.ID = employeeObject.GetAttribute("VI Person ID").Value;

            if (!keyOnly)
            {
                // Not yet implemented
                throw new NotImplementedException("Constructing Full EmployeeType not supported!");
            }

            // Finished
            return employeeType;
        }
    }
}