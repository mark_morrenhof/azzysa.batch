﻿using System;

namespace Azzysa.Batch.Web.ENOVIA.Models.Canonical
{
    public static class CanonicalExtensions
    {

        public static BusinessObjectAttribute GetAttribute(this BusinessObject businessObject, string attributeName)
        {
            var att = businessObject.Attributes.Find(a => a.Name.Equals(attributeName, System.StringComparison.InvariantCulture));
            if (att == null)
            {
                throw new ApplicationException($"Unable to attribute '{attributeName}' for {businessObject.Type}, {businessObject.Name}, {businessObject.Revision}!");
            }
            return att;
        }

        public static BusinessObjectAttribute GetAttribute(this Relationship relationship, string attributeName)
        {
            var att = relationship.Attributes.Find(a => a.Name.Equals(attributeName, System.StringComparison.InvariantCulture));
            if (att == null)
            {
                throw new ApplicationException($"Unable to attribute '{attributeName}' for {relationship.RelationshipType}, {relationship.RelationShipId}!");
            }
            return att;
        }
    }
}
