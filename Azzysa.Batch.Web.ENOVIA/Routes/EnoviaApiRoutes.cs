﻿using Azzysa.Batch.Web.Routes;

namespace Azzysa.Batch.Web.ENOVIA.Routes
{
    internal enum EnoviaApiRoutes
    {
        #region IBC

        [ApiRoute("VIServiceModeler/ibc/get/partgroup/part")]
        GetPartGroupWithParts = 10001,
        [ApiRoute("VIServiceModeler/ibc/get/partgroup/dynamicattribute")]
        GetPartGroupWithDynamicAttributes = 10002,
        [ApiRoute("VIServiceModeler/ibc/update/partgroup")]
        UpdatePartGroup = 10101,

        [ApiRoute("VIServiceModeler/ibc/get/part")]
        GetPart = 20001,
        [ApiRoute("VIServiceModeler/ibc/update/part")]
        UpdatePart = 20102,

        [ApiRoute("VIServiceModeler/ibc/get/cadmodel/related")]
        GetCADModelWithRelatedComponents = 30001,

        [ApiRoute("VIServiceModeler/ibc/get/dynamicattribute/available")]
        GetAvailableDynamicAttributes = 50001,

        [ApiRoute("VIServiceModeler/ibc/get/revision/latest")]
        GetLatestRevision = 60001,

        #endregion

        #region Canonical

        [ApiRoute("VIServiceModeler/canonical/messageData")]
        GetCanonicalData = 70001,

        #endregion

        #region File

        [ApiRoute("VIServiceModeler/api/get/fcs/ticket/checkout")]
        GetFcsTicketCheckout = 80001,

        [ApiRoute("VIServiceModeler/api/get/fcs/ticket/checkin")]
        GetFcsTicketCheckin = 80002,

        [ApiRoute("VIServiceModeler/api/complete/fcs/checkin")]
        CompleteCheckin = 80003,

        [ApiRoute("VIServiceModeler/api/get/relatedFiles")]
        GetRelatedFiles = 80004,

        #endregion
    }
}
