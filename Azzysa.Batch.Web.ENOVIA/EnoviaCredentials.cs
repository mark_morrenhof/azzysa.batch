﻿namespace Azzysa.Batch.Web.ENOVIA
{
    public class EnoviaCredentials : IEnoviaCredentials
    {
        public string CollaborativeSpace { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string Vault { get; set; }
    }
}
