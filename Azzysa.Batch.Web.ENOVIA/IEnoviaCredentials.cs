﻿namespace Azzysa.Batch.Web.ENOVIA
{
    public interface IEnoviaCredentials
    {
        string UserName { get; set; }
        string Password { get; set; }
        string Vault { get; set; }
        string CollaborativeSpace { get; set; }
    }
}
