﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Azzysa.Batch.Kernel;
using Azzysa.Batch.Plugins.PDFConverterPlugin;
using Azzysa.Batch.Plugins.TestPlugin;
using Azzysa.Batch.Web;
using Azzysa.Batch.Web.Models.Engineering;
using Azzysa.Batch.Web.Models.Parameters;
using Newtonsoft.Json;

namespace Azzysa.Batch.Plugin.Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            //var plugin = new PdfConverterPlugin();
            //var parameters = new PdfConversionParameters()
            //{
            //    ObjectType = "Document",
            //    ObjectName = "TestDocument",
            //    ObjectRevision = "0",
            //    OutputFileName = "result.pdf",
            //    OutputFormat = "doc",
            //    SourceFileName = "Source.xlsx",
            //    SourceFormat = "generic",
            //};

            var parameters = new RelatedFilesParameters
            {
                Part = new Part("021000-011-00001", "A01"),
                Types =  new List<string>() { "SW Component Family" }
            };

            var plugin = new TestPlugin();

            var operationMessageParameters = JsonConvert.SerializeObject(parameters);

            plugin.Execute(new OperationMessage { Parameters = operationMessageParameters }, BatchConnection.Register(@"http://localhost/Batch.Api/"));
        }
    }
}
