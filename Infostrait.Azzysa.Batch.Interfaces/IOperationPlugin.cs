﻿using Azzysa.Batch.Web;

namespace Azzysa.Batch.Interfaces
{

    /// <summary>
    /// Interface to be implemented in order to be able to use in processing <see cref="IOperationMessage"/> objects by the Batch Kernel
    /// </summary>
    public interface IOperationPlugin
    {

        /// <summary>
        /// Returns the name of the IOperationPlugin
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Returns operations upparted by plugin
        /// </summary>
        string Operation { get; }

        /// <summary>
        /// IOperationPlugin implementation implements this method to do the actual processing of a an object
        /// </summary>
        /// <param name="operationMessage">Message with details that has to be processed by the IOperationPlugin implementation</param>
        /// <param name="connector"><see cref="IBatchConnector"/> connector used by plugin</param>
        /// <param name="fileClient"></param>
        void Execute(IOperationMessage operationMessage, IBatchConnector connector);
    }
}