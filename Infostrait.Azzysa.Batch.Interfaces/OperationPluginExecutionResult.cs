﻿namespace Azzysa.Batch.Interfaces
{

    /// <summary>
    /// Represents the <see cref="IOperationPlugin.Execute"/> results
    /// </summary>
    public enum OperationPluginExecutionResult
    {
        
        /// <summary>
        /// Plugin execution failed, errors are logged and handled by the plugin
        /// </summary>
        Failed,

        /// <summary>
        /// Plugin execution is succeeded, warnings or messages are loged by the plugin
        /// </summary>
        Succeed

    }
}