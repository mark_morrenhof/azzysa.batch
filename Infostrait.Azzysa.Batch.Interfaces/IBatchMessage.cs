﻿using System.Collections.Generic;

namespace Azzysa.Batch.Interfaces
{

    /// <summary>
    /// Plugin message to be processed by a <see cref="IOperationPlugin"/> implementation
    /// </summary>
    public interface IBatchMessage
    {
        
        /// <summary>
        /// Gets or sets the friendly name of the message
        /// </summary>
        string Label { get; set; } 

        /// <summary>
        /// A name/value dictionary of properties for this message
        /// </summary>
        Dictionary<string, string> Properties { get; set; }

        /// <summary>
        /// Operation to be performed on the file
        /// </summary>
        string Operation { get; set; }

    }
}