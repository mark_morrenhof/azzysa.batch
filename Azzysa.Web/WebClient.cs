﻿using System;
using System.Net;

namespace Azzysa.Web
{
    public class WebClient : Web.WebClient, IDisposable
    {
        private Infostrait.Azzysa.Interfaces.IConnectionLock connectionLock;
        private IEnoviaCredentials credentials;

        public WebClient(string url, IEnoviaCredentials credentials = null) : base(url)
        {
            this.credentials = credentials ?? EnoviaConnection.DefaultCredentials;
        }

        public override T Get<T>(ApiRoute route)
        {
            try
            {
                return base.Get<T>(route);
            }
            finally
            {
                if (connectionLock != null)
                    connectionLock.Dispose();
            }
        }

        public override T Post<T>(ApiRoute route, object obj)
        {
            try
            {
                return base.Post<T>(route, obj);
            }
            finally
            {
                if (connectionLock != null)
                    connectionLock.Dispose();
            }
        }

        public void Dispose()
        {
            if (connectionLock != null)
                connectionLock.Dispose();
        }

        public override HttpWebRequest CreateRequest(Uri url)
        {
            connectionLock = EnoviaConnection.Runtime.ConnectionPool.GetPoolObject(EnoviaConnection.Runtime.ServicesUrl, credentials.UserName, credentials.Password, credentials.Vault);
            var client = connectionLock.GetConnection().GetWebClient() as ExtendedWebClient;

            if (client == null)
                throw new ApplicationException("Unable to get initialized web client");

            return (HttpWebRequest)client.GetRequest(url);
        }
    }
}
