﻿using System.Collections.Generic;

namespace Azzysa.Web.Models.Canonical
{
    public class Relationship
    {

        //[JsonProperty("FromObject")]
        public BusinessObject FromObject { get; set; }
        public BusinessObject ToObject { get; set; }
        public string RelationshipType { get; set; }
        public string RelationShipId { get; set; }
        public string direction { get; set; }
        public List<BusinessObjectAttribute> Attributes { get; set; }

        public Relationship()
        {
            this.Attributes = new List<BusinessObjectAttribute>();
        }
    }
}
