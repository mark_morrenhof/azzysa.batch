﻿using System.Collections.Generic;

namespace Azzysa.Web.Models.Canonical
{
    public class BusinessObject
    {
        public List<BusinessObjectAttribute> Attributes { get; set; }
        public List<Relationship> ConnectedObjects { get; set; }
        public string ObjectId { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Revision { get; set; }
        public string Policy { get; set; }
        public string Vault { get; set; }
        public string Description { get; set; }
        public string ParentRevision { get; set; }
        public object revisionStage { get; set; }
        public string State { get; set; }
        public object CreationDate { get; set; }
        public object ModificationDate { get; set; }
        public bool IsLatest { get; set; }

        public BusinessObject()
        {
            this.Attributes = new List<BusinessObjectAttribute>();
            this.ConnectedObjects = new List<Relationship>();
        }
    }
}
