﻿namespace Azzysa.Web.Models.Canonical
{
    public class BusinessObjectAttribute
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string InputValue { get; set; }
        public string InputUnit { get; set; }
        public string Unit { get; set; }

        public BusinessObjectAttribute()
        {
        }
    }
}