﻿namespace Azzysa.Web.Interfaces
{
    public interface IEnoviaConnector
    {
        IIBCConnector IBC { get; }
        ICanonicalConnector Canonical { get; }
    }
}
