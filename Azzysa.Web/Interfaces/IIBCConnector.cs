﻿using System.Collections.Generic;

namespace Azzysa.Web.Interfaces
{
    public interface IIBCConnector
    {
        #region Get PartGroup With Parts
        PartGroup GetPartGroupWithParts(string name, string revision, IEnoviaCredentials credentials = null);
        PartGroup GetPartGroupWithParts(PartGroup pg, IEnoviaCredentials credentials = null);
        List<PartGroup> GetPartGroupsWithParts(List<PartGroup> list, IEnoviaCredentials credentials = null);
        #endregion

        #region Get CADModel With Related Components
        CADModel GetCADModelWithRelatedComponents(string type, string name, string revision, IEnoviaCredentials credentials = null);
        CADModel GetCADModelWithRelatedComponents(CADModel drw, IEnoviaCredentials credentials = null);
        List<CADModel> GetCADModelsWithRelatedComponents(List<CADModel> list, IEnoviaCredentials credentials = null);
        #endregion

        #region Get Parts
        Part GetPart(string name, string revision = null, IEnoviaCredentials credentials = null);
        Part GetPart(Part part, IEnoviaCredentials credentials = null);
        List<Part> GetParts(List<Part> list, IEnoviaCredentials credentials = null);

        BaseComponent GetLatestRevision(string type, string name, IEnoviaCredentials credentials = null);
        BaseComponent GetLatestRevision(BaseComponent component, IEnoviaCredentials credentials = null);
        List<BaseComponent> GetLatestRevisions(List<BaseComponent> component, IEnoviaCredentials credentials = null);
        #endregion

        #region Dynamic Attributes
        List<DynamicAttributeDefinition> GetAvailableDynamicAttributes(IEnoviaCredentials credentials = null);
        PartGroup GetPartGroupWithDynamicAttributes(string name, string revision, IEnoviaCredentials credentials = null);
        PartGroup GetPartGroupWithDynamicAttributes(PartGroup pg, IEnoviaCredentials credentials = null);
        List<PartGroup> GetPartGroupsWithDynamicAttributes(List<PartGroup> list, IEnoviaCredentials credentials = null);
        #endregion

        #region Update
        void Update(PartGroup pg, IEnoviaCredentials credentials = null);
        void Update(List<PartGroup> list, IEnoviaCredentials credentials = null);
        void Update(Part part, IEnoviaCredentials credentials = null);
        void Update(List<Part> list, IEnoviaCredentials credentials = null);
        #endregion
    }
}
