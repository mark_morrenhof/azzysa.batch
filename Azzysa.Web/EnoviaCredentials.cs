﻿using Vanderlande.Web;

namespace Azzysa.Web
{
    public class EnoviaCredentials : IEnoviaCredentials
    {
        public string CollaborativeSpace { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string Vault { get; set; }
    }
}
