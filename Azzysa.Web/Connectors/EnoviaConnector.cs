﻿using Azzysa.Web.Interfaces;

namespace Azzysa.Web.Connectors
{
    internal class EnoviaConnector : IEnoviaConnector
    {
        public IIBCConnector IBC { get; internal set;  }
        public ICanonicalConnector Canonical { get; internal set; }

        public EnoviaConnector(string url)
        {
            IBC = new IBCConnector(url);
            Canonical = new CanonicalConnector(url);
        }
    }
}
