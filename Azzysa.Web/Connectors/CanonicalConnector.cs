﻿using System;
using System.Collections.Generic;
using System.Net;
using Azzysa.Web.Interfaces;
using Azzysa.Web.Routes;

namespace Azzysa.Web.Connectors
{
    public class CanonicalConnector : ICanonicalConnector
    {
        private string url;

        internal CanonicalConnector(string url)
        {
            this.url = url;

            #region Routes
            //Canonical Data
            ApiRouteCollection.Add(EnoviaApiRoutes.GetCanonicalData, ApiRouteArguments.Type, ApiRouteArguments.Name, ApiRouteArguments.Revision);
            #endregion
        }

        #region Interface IEnoviaCanonicalConnector Implementation
        public List<Models.Canonical.BusinessObject> GetCanonicalMessageData(string type, string name, string revision, IEnoviaCredentials credentials = null)
        {
            var client = new WebClient(url, credentials);
            return client.Get<List<Models.Canonical.BusinessObject>>(ApiRouteCollection.GetRoute(EnoviaApiRoutes.GetCanonicalData, type, name, revision));
        }

        public DataType GetCanonicalMessage(string type, string name, string revision, IEnoviaCredentials credentials = null)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
