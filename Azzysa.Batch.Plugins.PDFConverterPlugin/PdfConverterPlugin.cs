﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Azzysa.Batch.Interfaces;
using Azzysa.Batch.Plugins.PDFConverterPlugin.Converters;
using Azzysa.Batch.Web;
using Azzysa.Batch.Web.Extensions;
using Azzysa.Batch.Web.Models.Files;
using Azzysa.Batch.Web.Models.Parameters;
using WebClient = System.Net.WebClient;

namespace Azzysa.Batch.Plugins.PDFConverterPlugin
{
    public class PdfConverterPlugin : IOperationPlugin
    {
        public string Name { get; }
        public string Operation => "pdf";

        private ConverterFactory _converterFactory = new ConverterFactory();

        public PdfConverterPlugin()
        {
            _converterFactory.Register(new WordConverter());
            _converterFactory.Register(new ExcelConverter());
            _converterFactory.Register(new PowerPointConverter());
            _converterFactory.Register(new VisioConverter());
        }

        public void Execute(IOperationMessage operationMessage, IBatchConnector connector)
        {
            var parameters = operationMessage.GetParameters<PdfConversionParameters>();

            var fcsChekoutRequest = new FcsCheckoutRequest()
            {
                Files = new List<FileData>() { new FileData() { ObjectType = parameters.ObjectType, ObjectName = parameters.ObjectName, ObjectRevision = parameters.ObjectRevision, Name = parameters.SourceFileName, Format = parameters.SourceFormat } },
            };

            using (var fileStream = connector.CheckOutFile(fcsChekoutRequest))
            using (var outputStream = new MemoryStream())
            {
                var extension = Path.GetExtension(parameters.SourceFileName);

                var converter = _converterFactory.GetConverter(extension);
                converter.Convert(fileStream, extension, outputStream);

                var fcsCheckinRequest = new FcsCheckinRequest()
                {
                    Files = new List<FileData>() { new FileData() { ObjectType = parameters.ObjectType, ObjectName = parameters.ObjectName, ObjectRevision = parameters.ObjectRevision, Name = parameters.OutputFileName, Format = parameters.OutputFormat } },
                };

                connector.CheckInFile(outputStream, fcsCheckinRequest);
            }
        }
    }
}
