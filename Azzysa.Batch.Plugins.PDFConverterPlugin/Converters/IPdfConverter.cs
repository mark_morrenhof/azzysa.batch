﻿using System.Collections.Generic;
using System.IO;

namespace Azzysa.Batch.Plugins.PDFConverterPlugin.Converters
{
    public interface IPdfConverter
    {
        void Convert(Stream inputStream, string extension, string destination);
        void Convert(Stream inputStream, string extension, Stream outputStream);
        void Convert(string source, string destination);
        IEnumerable<string> SupportedExtensions { get; }
    }
}