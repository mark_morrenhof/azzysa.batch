﻿using System;
using System.Collections.Generic;
using System.IO;
using Aspose.Cells;

namespace Azzysa.Batch.Plugins.PDFConverterPlugin.Converters
{
    public class ExcelConverter : PdfConverterBase
    {
        readonly Aspose.Cells.License _license = new Aspose.Cells.License();

        public ExcelConverter()
        {
            try
            {
                _license.SetLicense(LicenseLocation);
            }
            catch (Exception e)
            {
                // todo handel license load error
            }
        }

        public override void Convert(Stream inputStream, string extension, Stream outputStream)
        {
            var loadOptions = new LoadOptions
            {
                MemorySetting = MemorySetting.MemoryPreference,
            };
            var document = new Workbook(inputStream, loadOptions);
            document.Save(outputStream, SaveFormat.Pdf);
        }

        public override IEnumerable<string> SupportedExtensions => new List<string> {".xlsx", ".xls", ".csv", ".xlsm", ".xltx", ".xlsb", ".ods",};
    }
}