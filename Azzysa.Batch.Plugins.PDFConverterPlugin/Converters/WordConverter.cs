﻿using System;
using System.Collections.Generic;
using System.IO;
using Aspose.Words;


namespace Azzysa.Batch.Plugins.PDFConverterPlugin.Converters
{
    public class WordConverter : PdfConverterBase
    {
        Aspose.Words.License _license = new Aspose.Words.License();

        public WordConverter()
        {
            try
            {
                _license.SetLicense(LicenseLocation);
            }
            catch (Exception e)
            {
                // todo handel license load error
            }
        }

        public override void Convert(Stream inputStream, string extension, Stream outputStream)
        {
            var document = new Document(inputStream);

            document.UpdateFields();

            document.Save(outputStream, SaveFormat.Pdf);
        }

        public override IEnumerable<string> SupportedExtensions => new List<string>() { ".docx", ".doc", ".docm", ".rtf", ".dot", ".dotx", ".html", ".mhtml", ".odt",};
    }
}