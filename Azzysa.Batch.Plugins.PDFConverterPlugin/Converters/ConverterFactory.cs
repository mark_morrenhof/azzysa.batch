﻿using System.Collections.Generic;

namespace Azzysa.Batch.Plugins.PDFConverterPlugin.Converters
{
    public class ConverterFactory
    {
        private readonly Dictionary<string, IPdfConverter> _converterMap = new Dictionary<string, IPdfConverter>();

        public void Register(IPdfConverter converter)
        {
            foreach (var ext in converter.SupportedExtensions)
            {
                _converterMap.Add(ext, converter);
            }
        }

        public IPdfConverter GetConverter(string extension)
        {
            IPdfConverter converter = null;
            _converterMap.TryGetValue(extension, out converter);
            return converter;
        }
    }
}