﻿using System;
using System.Collections.Generic;
using System.IO;
using Aspose.Diagram;

namespace Azzysa.Batch.Plugins.PDFConverterPlugin.Converters
{
    public class VisioConverter : PdfConverterBase
    {
        readonly Aspose.Diagram.License _license = new Aspose.Diagram.License();

        public VisioConverter()
        {
            try
            {
                _license.SetLicense(LicenseLocation);
            }
            catch (Exception e)
            {
                // todo handel license load error
            } 
        }

        public override void Convert(Stream inputStream, string extension, Stream outputStream)
        {
            var document = new Diagram(inputStream, _extensionsWithFileFormats[extension]);

            document.Save(outputStream, SaveFileFormat.PDF);
        }

        public override IEnumerable<string> SupportedExtensions => _extensionsWithFileFormats.Keys;

        private readonly Dictionary<string, Aspose.Diagram.LoadFileFormat> _extensionsWithFileFormats = new Dictionary<string, Aspose.Diagram.LoadFileFormat>()
        {
            { ".vsd", Aspose.Diagram.LoadFileFormat.VSD },
            { ".vsdx", Aspose.Diagram.LoadFileFormat.VSDX },
            { ".vdx", Aspose.Diagram.LoadFileFormat.VDX },
            { ".vdw", Aspose.Diagram.LoadFileFormat.VDW },
            { ".vss", Aspose.Diagram.LoadFileFormat.VSS },
            { ".vst", Aspose.Diagram.LoadFileFormat.VST },
            { ".vsx", Aspose.Diagram.LoadFileFormat.VSX },
            { ".vtx", Aspose.Diagram.LoadFileFormat.VTX },
        };
    }
}