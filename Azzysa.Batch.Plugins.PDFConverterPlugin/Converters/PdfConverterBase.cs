﻿using System.Collections.Generic;
using System.IO;

namespace Azzysa.Batch.Plugins.PDFConverterPlugin.Converters
{
    public abstract class PdfConverterBase : IPdfConverter
    {
        protected string LicenseLocation = @"Aspose.Total.lic";

        public void Convert(Stream inputStream, string extension, string destination)
        {
            using (var outputStream = File.Create(destination))
            {
                Convert(inputStream, extension, outputStream);
            }
        }

        public void Convert(string source, string destination)
        {
            using (var stream = File.OpenRead(source))
            {
                Convert(stream, Path.GetExtension(stream.Name), destination);
            }
        }

        public abstract void Convert(Stream inputStream, string extension, Stream outputStream);

        public abstract IEnumerable<string> SupportedExtensions { get; }
    }
}