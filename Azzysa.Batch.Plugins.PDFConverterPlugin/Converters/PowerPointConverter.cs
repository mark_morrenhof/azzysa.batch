﻿using System;
using System.Collections.Generic;
using System.IO;
using Aspose.Slides.Export;

namespace Azzysa.Batch.Plugins.PDFConverterPlugin.Converters
{
    public class PowerPointConverter : PdfConverterBase
    {
        readonly Aspose.Slides.License _license = new Aspose.Slides.License();

        public PowerPointConverter()
        {
            try
            {
                _license.SetLicense(LicenseLocation);
            }
            catch (Exception e)
            {
                // todo handel license load error
            }
        }
        public override void Convert(Stream inputStream, string extension, Stream outputStream)
        {

            Aspose.Slides.IPresentationInfo info = Aspose.Slides.PresentationFactory.Instance.GetPresentationInfo(inputStream);
            if (info.LoadFormat == Aspose.Slides.LoadFormat.Unknown)
                throw new ArgumentException("Unsupported presentation format");
            if (info.IsEncrypted)
                throw new ArgumentException("Presentation is encrypted");
            inputStream.Position = 0;
            var document = new Aspose.Slides.Presentation(inputStream);

            document.Save(outputStream, SaveFormat.Pdf);
        }

        public override IEnumerable<string> SupportedExtensions => new List<string>() { ".pptx", ".ppt", ".ppsx", ".pps", ".odp", };
    }
}