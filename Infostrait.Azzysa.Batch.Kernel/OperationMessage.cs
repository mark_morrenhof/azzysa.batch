﻿using Azzysa.Batch.Interfaces;
using Azzysa.Batch.Web;

namespace Azzysa.Batch.Kernel
{
    /// <summary>
    /// Message used by <see cref="IOperationPlugin"/> containing all the information to perform the operation
    /// </summary>
    public class OperationMessage : IOperationMessage
    {
        /// <summary>
        /// DOperation to be performed
        /// </summary>
        public string Operation { get; set; }

        /// <summary>
        /// Site
        /// </summary>
        public string Site { get; set; }

        /// <summary>
        /// Json string representing a generic set of parameters
        /// </summary>
        public string Parameters { get; set; }
    }
}
