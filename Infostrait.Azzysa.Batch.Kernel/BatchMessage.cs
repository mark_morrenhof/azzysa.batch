﻿using System.Collections.Generic;
using Azzysa.Batch.Interfaces;

namespace Azzysa.Batch.Kernel
{
    /// <summary>
    /// 
    /// </summary>
    public class BatchMessage : IBatchMessage
    {

        /// <summary>
        /// Gets or sets the friendly name of the message
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// A name/value dictionary of properties for this message
        /// </summary>
        public Dictionary<string, string> Properties { get; set; }

        /// <summary>
        /// Operation to be performed on the file
        /// </summary>
        public string Operation { get; set; }
    }
}