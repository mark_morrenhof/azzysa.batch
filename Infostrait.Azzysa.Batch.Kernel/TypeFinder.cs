﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Azzysa.Batch.Kernel
{

    /// <summary>
    /// Supports the <see cref="PluginLoader"/> object for collecting the requested types according a type
    /// </summary>
    internal class TypeFinder
    {

        /// <summary>
        /// Default constructor
        /// </summary>
        internal TypeFinder()
        {
            // Default constructor
        }

        /// <summary>
        /// Search for assembly types matching the given type interface in a specific path/directory.
        /// </summary>
        /// <param name="path">Folder to search in</param>
        /// <param name="filter">File filter, for example: *.dll</param>
        /// <param name="type">Interface type to search for</param>
        /// <returns>Collection of found types</returns>
        /// <exception cref="DirectoryNotFoundException">When the specified directory does not exists</exception>
        internal TypeFinderResult SearchPath(string path, string filter, Type type)
        {
            var returnValue = new TypeFinderResult();

            // Check type search location
            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException(path);
            }

            // Iterate through the path
            foreach (var file in Directory.GetFiles(path, filter))
            {
                // Implement exception handling over here since we try to load a type that could result in an exception
                try
                {
                    // Load assembly into memory
                    var assemblyObject = Assembly.LoadFrom(file);

                    // Iterate through its type to search for the requested interface type
                    foreach (var typeObject in assemblyObject.GetTypes())
                    {
                        // Check the interfaces that the current type is exposing and compare this with the requested type
                        if(type.IsAssignableFrom(typeObject) && typeObject.IsClass && !typeObject.IsAbstract)
                        { 
                            // Interface is the same as the requested interface
                            returnValue.FoundTypes.Add(new FoundType() {Assembly = assemblyObject, FileInfo = new FileInfo(Path.Combine(path, file)), Type = typeObject});
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Add type/assembly load exception to the result if not already there
                    if (!returnValue.ErrorDictionary.ContainsKey(file))
                    {
                        returnValue.ErrorDictionary.Add(file, ex);
                    }
                }
            }
            
            // Finished
            return returnValue;
        }
    }
}