﻿using System;
using System.Collections.Generic;

namespace Azzysa.Batch.Kernel
{

    /// <summary>
    /// Internal class that supports <see cref="TypeFinder"/> holding type find results
    /// </summary>
    internal class TypeFinderResult
    {
        
        /// <summary>
        /// Gets or sets the list of found types
        /// </summary>
        internal List<FoundType> FoundTypes { get; set; }

        /// <summary>
        /// Gets or sets the collection of type/assembly load exception
        /// </summary>
        internal Dictionary<string, Exception> ErrorDictionary { get; set; }

        internal TypeFinderResult()
        {
            FoundTypes = new List<FoundType>();
            ErrorDictionary = new Dictionary<string, Exception>();
        }

    }
}