﻿using System;
using System.Collections.Generic;
using System.Linq;
using Azzysa.Batch.Interfaces;

namespace Azzysa.Batch.Kernel 
{

    /// <summary>
    /// Manages all <see cref="IOperationPlugin"/> objects
    /// </summary>
    public class PluginLoader : IDisposable
    {
        // Private members

        /// <summary>
        /// Gets or sets the list of available plugins
        /// </summary>
        public List<IOperationPlugin> Plugins { get; set; }

        /// <summary>
        /// Gets the list of operations supported by the plugins
        /// </summary>
        public IEnumerable<string> Operations {
            get { return Plugins.Select(p => p.Operation); }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public PluginLoader()
        {
            // Default constructor
            Plugins = new List<IOperationPlugin>();
        }

        /// <summary>
        /// Search for plugins and load the plugins that are found.
        /// </summary>
        /// <param name="pluginLocation">Location where all <see cref="IOperationPlugin"/> assemblies are stored</param>
        /// <exception cref="ArgumentNullException">If runtime parameter is not set a valid instance</exception>
        /// <exception cref="System.IO.DirectoryNotFoundException">When the specified directory does not exists</exception>
        public void Initialize(string pluginLocation)
        {
            // Check runtime parameter

            // Search for IOperationPlugin types
            var typeFinder = new TypeFinder();
            var typeFinderResults = typeFinder.SearchPath(pluginLocation, "Azzysa.Batch.Plugins.*.dll", typeof (IOperationPlugin));

            // Check if any exception
            if (typeFinderResults.ErrorDictionary.Any())
            {
                // Type load contains errors
                throw new PluginLoadException("Type load exception occured when initializing the PluginLoader (IOperationPlugin)", typeFinderResults);
            }

            // Load all IOperationPlugin objects from the result
            foreach (var pluginType in typeFinderResults.FoundTypes)
            {
                Plugins.Add((IOperationPlugin) Activator.CreateInstance(pluginType.Type));
            }
            
            // Finished
        }

        /// <summary>
        /// Cleanup private members
        /// </summary>
        public void Dispose()
        {
            Plugins.Clear();
            Plugins = null;
        }

    }
}