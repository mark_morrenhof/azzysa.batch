﻿using System;
using System.Collections.Generic;
using System.Linq;
using Azzysa.Batch.Interfaces;

namespace Azzysa.Batch.Kernel
{
    internal class PluginLoadException : Exception
    {

        // Private members
        private Dictionary<string, Exception> Errors;

        /// <summary>
        /// Exception that occurs when loading the <see cref="IOperationPlugin"/> assemblies
        /// </summary>
        /// <param name="message">Describing message</param>
        /// <param name="result"><see cref="TypeFinderResult"/> containing all errors that occured</param>
        internal PluginLoadException(string message, TypeFinderResult result) : base(message)
        {
            if (result != null)
            {
                Errors = result.ErrorDictionary;
            }
            else
            {
                Errors = new Dictionary<string, Exception>();
            }
        }

        public override string ToString()
        {
            // Create message header
            var returnMessage = Message + Environment.NewLine + Environment.NewLine + "Errors occured: " + Environment.NewLine;

            // Add type load errors
            returnMessage = Errors.Aggregate(returnMessage, (current, error) => current + (error.Key + Environment.NewLine + error.Value.ToString()));

            // Finished, return the result
            return returnMessage;
        }
    }
}