﻿using System;
using System.IO;
using System.Reflection;

namespace Azzysa.Batch.Kernel
{
    /// <summary>
    /// Internal class that support <see cref="TypeFinder"/> and <see cref="TypeFinderResult"/>
    /// </summary>
    internal class FoundType
    {
        /// <summary>
        /// The <see cref="System.Type"/> found
        /// </summary>
        internal Type Type { get; set; }

        /// <summary>
        /// The <see cref="FileInfo"/> for the assembly that contains the <see cref="System.Type"/>
        /// </summary>
        internal FileInfo FileInfo { get; set; }

        /// <summary>
        /// The <see cref="Assembly"/> that exposes the <see cref="System.Type"/> that implements the requested type
        /// </summary>
        internal Assembly Assembly { get; set; }

    }
}