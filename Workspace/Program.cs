﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Azzysa.Batch.Web.ENOVIA;
using Azzysa.Batch.Web.ENOVIA.Interfaces;
using Azzysa.Batch.Web.Exceptions;
using Azzysa.Batch.Web.Models.Engineering;
using Azzysa.Batch.Web.Models.Files;

namespace Workspace
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                /* Enovia */
                var connector = GetEnoviaConnector();

                var a = connector.File.GetPartWithRelatedFiles(new Part("0AP023-010-11000", "A01"), new List<string>());

                //var b = connector.IBC.GetPartGroupsWithParts(new List<PartGroup> { new PartGroup("0210ff00-158", "A01") });

                //var i = connector.IBC.GetAvailableDynamicAttributes();

                //var a = connector.IBC.GetPartGroupWithParts("000110-108", "A01");
                //var b = connector.IBC.GetPartGroupsWithParts(new List<PartGroup> { new PartGroup("021000-158", "A01"), new PartGroup("021000-158", "A01"), new PartGroup("021000-158", "A01"), new PartGroup("021000-158", "A01"), new PartGroup("021000-158", "A01") });
                //var b2 = connector.IBC.GetPartGroupsWithParts(new List<PartGroup> { new PartGroup("021000-158", "A01") });
                //var c = connector.IBC.GetCADModelWithRelatedComponents("SW Assembly Family", "A-0000358", "---");
                //var d = connector.IBC.GetCADModelsWithRelatedComponents(new List<CADModel>() { new CADModel("SW Assembly Family", "A-0000358", "---") });
                //var e = connector.IBC.GetPart("021000-158-00001", "A01");
                //var f = connector.IBC.GetParts(new List<Part> { new Part("021000-158-00001", "A01") });
                //var g = connector.IBC.GetLatestRevision(Part.TypeName, "011584-530-00001");
                //var h = connector.IBC.GetLatestRevisions(new List<BaseComponent> { new Part("011584-530-00001", null) });

                //connector.IBC.Update(new List<Part> { new Part("021000-158-00001", "A01") });
                //connector.IBC.Update(new List<PartGroup> { new PartGroup("021000-158", "A01") });
            }
            catch (AzzysaWebException exc)
            {
                Console.WriteLine(exc.Message);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }

        #region Enovia Connector
        public static IEnoviaConnector GetEnoviaConnector()
        {
            #region Parameters
            var connectionString = "http://localhost:8070/internal/resources";
            var serverName = "VI109006";
            var siteName = "nlveg";
            var rootPath = @"D:\Projects\Azzysa";
            var userName = "admin_platform";
            var password = "Passport1";
            var vault = "eService Production";
            #endregion

            return EnoviaConnection.Register(connectionString, serverName, siteName, rootPath, userName, password, vault);
        }
        #endregion
    }
}
