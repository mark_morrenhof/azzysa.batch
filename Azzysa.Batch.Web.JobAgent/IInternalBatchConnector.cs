﻿namespace Azzysa.Batch.Web.JobAgent
{
    public interface IInternalBatchConnector
    {
        bool Register(IJobAgentInfo jobAgentInfo);
    }
}