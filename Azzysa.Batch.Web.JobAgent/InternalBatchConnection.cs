﻿namespace Azzysa.Batch.Web.JobAgent
{
    public class InternalBatchConnection
    {
        public static IInternalBatchConnector Register(string url)
        {
            return InternalBatchConnector.CreateInstance(url);
        }
    }
}