﻿using System;
using Azzysa.Batch.Web.JobAgent.Routes;
using Azzysa.Batch.Web.Routes;

namespace Azzysa.Batch.Web.JobAgent
{
    public class InternalBatchConnector : IInternalBatchConnector
    {
        private WebClient client;

        internal static InternalBatchConnector Instance { get; set; }

        public InternalBatchConnector(string url)
        {
            this.client = new WebClient(url);

            ApiRouteCollection.Add(InternalBatchApiRoutes.Register);
        }

        public static IInternalBatchConnector CreateInstance(string url)
        {
            Instance = new InternalBatchConnector(url);
            return Instance;
        }

        public bool Register(IJobAgentInfo jobAgentInfo)
        {
            try
            {
                return client.Post<bool>(ApiRouteCollection.GetRoute(InternalBatchApiRoutes.Register), jobAgentInfo);
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}