﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Azzysa.Batch.Web.Routes;

namespace Azzysa.Batch.Web.JobAgent.Routes
{
    enum InternalBatchApiRoutes
    {
        [ApiRoute("api/jobagent/register")]
        Register = 10001,
    }
}
