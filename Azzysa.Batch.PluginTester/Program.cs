﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services.Description;
using Azzysa.Batch.Plugins.PDFConverterPlugin;
using Azzysa.Batch.Web;
using Azzysa.Batch.Web.Models.Parameters;

namespace Azzysa.Batch.PluginTester
{
    class Program
    {
        static void Main(string[] args)
        {
            var plugin = new PdfConverterPlugin();
            var connector = BatchConnection.Register("http://localhost/Batch.Api/");
            var operationMessage = OperationMessage.Create("Veghel", "pdf", new PdfConversionParameters {Source = @"D:\Test\Test.docx", Destination = @"D:\Test\Test.pdf" });

            plugin.Execute(operationMessage, connector);
        }
    }
}
