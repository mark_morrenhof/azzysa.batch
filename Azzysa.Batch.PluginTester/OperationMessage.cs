﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Azzysa.Batch.Web;
using Azzysa.Batch.Web.Models.Parameters;
using Newtonsoft.Json;

namespace Azzysa.Batch.PluginTester
{
    public class OperationMessage : IOperationMessage
    {
        public string Operation { get; set; }
   
        public string Parameters { get; set; }
        
        public string Site { get; set; }

        private OperationMessage()
        {
        }

        public static IOperationMessage Create<T>(string site, string operation, T parameters) {
            return new OperationMessage()
            {
                Site = site,
                Operation = operation,
                Parameters = JsonConvert.SerializeObject(parameters),
            };
        }
    }
}
